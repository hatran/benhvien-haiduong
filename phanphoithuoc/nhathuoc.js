var datanhathuoc = [
 {
   "STT": 1,
   "Name": "CTCP Dược phẩm IQ pharma",
   "address": "100 Phan Đình Phùng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9506809,
   "Latitude": 106.3189106
 },
 {
   "STT": 2,
   "Name": "Công ty cổ phần dược phẩm và thương mại Ánh Dương",
   "address": "Số 43 Tuệ Tĩnh,  Phường  nguyễn Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.941327,
   "Latitude": 106.3255374
 },
 {
   "STT": 3,
   "Name": "CTCP Dược VTYT Hải Dương",
   "address": "Số 102 Chi Lăng, Phường   Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.944076,
   "Latitude": 106.3278
 },
 {
   "STT": 4,
   "Name": "CTCP Dược phẩm Đông Dương",
   "address": "Số 79 Quán Thánh, Phường  Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9501082,
   "Latitude": 106.3277489
 },
 {
   "STT": 5,
   "Name": "Công ty TNHHDP Thành Đông",
   "address": "Số 106 Bùi Thị Xuân, Phường  Lê Thanh Nghị, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9306702,
   "Latitude": 106.3300657
 },
 {
   "STT": 6,
   "Name": "Chi nhánh CTCP Dược Hậu Giang tại Hải Dương",
   "address": "Ki ốt 01, 265 Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9461429,
   "Latitude": 106.3236979
 },
 {
   "STT": 7,
   "Name": "CN CTCP XNK Y tế Domesco ",
   "address": "Lô 61.52, khu 10, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9201713,
   "Latitude": 106.333369
 },
 {
   "STT": 8,
   "Name": " Chi nhánh công ty cổ phần Traphaco tại Hải Dương",
   "address": "87.1.17 khu tây Nam Cường, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9332819,
   "Latitude": 106.2908179
 },
 {
   "STT": 9,
   "Name": "Chi nhánh TPHD- Công ty CP dược VTYT Hải Dương",
   "address": "số 144 Quang Trung, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9454237,
   "Latitude": 106.3351142
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Hải Châu",
   "address": "295 Nguyễn Trãi II, Thị trấnr Sao Đỏ, Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1130318,
   "Latitude": 106.3969466
 },
 {
   "STT": 11,
   "Name": "Nhà thuốc Minh Thành ",
   "address": "Số 8 Bùi Thị Xuân, Phường  Lê Thanh Nghị, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9327201,
   "Latitude": 106.3280699
 },
 {
   "STT": 12,
   "Name": "Nhà thuốc GPP Phúc Anh",
   "address": "Số 32 Lý Thường Kiệt, Phường   Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9370318,
   "Latitude": 106.3300547
 },
 {
   "STT": 13,
   "Name": "Nhà thuốc Việt Hùng ",
   "address": "Số 320 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9373368,
   "Latitude": 106.308853
 },
 {
   "STT": 14,
   "Name": "Nhà thuốc Minh Thư",
   "address": "Số 34D Tam Giang, Phường  Trần Hưng Đạo, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9364032,
   "Latitude": 106.340441
 },
 {
   "STT": 15,
   "Name": "Nhà thuốc Trường Xuân",
   "address": "Số 115 CHi Lăng, Phường   Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9429188,
   "Latitude": 106.3280636
 },
 {
   "STT": 16,
   "Name": "Nhà thuốc Đông Thoa",
   "address": "Ki ốt số 3 chợ Thanh Bình, Đường Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9321454,
   "Latitude": 106.2934608
 },
 {
   "STT": 17,
   "Name": "Nhà thuốc Phúc Nga",
   "address": "Số 98 phố Tuy Hòa, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9364079,
   "Latitude": 106.3289967
 },
 {
   "STT": 18,
   "Name": "Nhà thuốc Bình Minh",
   "address": "Ki ốt số 2 chợ Thanh Bình,Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9321454,
   "Latitude": 106.2934608
 },
 {
   "STT": 19,
   "Name": "Nhà thuốc bệnh viện  7- Cục hậu cần – Quân khu 3",
   "address": "Số 12 Tuệ Tĩnh, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9416612,
   "Latitude": 106.3263863
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Lý Thăng ",
   "address": "Khu xuất khẩu, Thị trấn Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8690417,
   "Latitude": 106.2937858
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Hưng Thu",
   "address": "Số 117 Mạc Thị Bưởi, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9341622,
   "Latitude": 106.3269663
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Phúc Thành",
   "address": "Số 1A Chi Lăng, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9475299,
   "Latitude": 106.3282019
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Thanh Bình",
   "address": "Ki ốt số 1, chợ Thanh Bình, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9321454,
   "Latitude": 106.2934608
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Thanh Mai",
   "address": "Số 97 Bình Minh, Phường  Phạm Ngũ Lão, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9324717,
   "Latitude": 106.3227854
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Tuấn Nam",
   "address": "Số 101C Yết Kiêu, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9181043,
   "Latitude": 106.3339735
 },
 {
   "STT": 26,
   "Name": "Nhà thuốc Đức Thảo ",
   "address": "Số 91 Hai Bà Trưng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9414858,
   "Latitude": 106.334905
 },
 {
   "STT": 27,
   "Name": "Nhà thuốc Kim Phương",
   "address": "Số 8 Đường Thanh Niên, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9470519,
   "Latitude": 106.336407
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Hồng Quang     ",
   "address": "Số 687B Lê Thanh Nghị, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9218605,
   "Latitude": 106.3294448
 },
 {
   "STT": 29,
   "Name": "Nhà thuốc Đông Đô",
   "address": "Số 316 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9360481,
   "Latitude": 106.293057
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Hoài Thanh ",
   "address": "Số 26 Nguyễn Chí Thanh, Phường   tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9328833,
   "Latitude": 106.3194501
 },
 {
   "STT": 31,
   "Name": "Nhà thuốc Ngọc Khánh I",
   "address": "Cổng BVĐK, số 225 Nguyễn Lương Bằng Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9353833,
   "Latitude": 106.3071555
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc Hồng Ngọc",
   "address": "Số 47 Tuệ Tĩnh, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9413042,
   "Latitude": 106.3254608
 },
 {
   "STT": 33,
   "Name": "Nhà thuốc Thái Bình",
   "address": "Số 221 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9370308,
   "Latitude": 106.307813
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Tuấn Hưng ",
   "address": "Số 170 Điện Biên Phủ, Phường  Phạm Ngũ Lão. Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9409612,
   "Latitude": 106.3207777
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc số 06-Hiệu thuốc TPHD",
   "address": "Cổng BVĐK tỉnh, 225 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9353833,
   "Latitude": 106.3071555
 },
 {
   "STT": 36,
   "Name": "Nhà thuốc Thanh Bình ",
   "address": "Số 655 Lê Thanh Nghị, Phường  Lê Thanh Nghị, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9236938,
   "Latitude": 106.3289054
 },
 {
   "STT": 37,
   "Name": "Nhà thuốc Đa Khoa",
   "address": "Cổng BVĐK, số 225 Nguyễn Lương Bằng Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9353833,
   "Latitude": 106.3071555
 },
 {
   "STT": 38,
   "Name": " Nhà thuốc Thanh Phương",
   "address": "125H Quang Trung, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9446175,
   "Latitude": 106.3345795
 },
 {
   "STT": 39,
   "Name": "Nhà thuốc Trung Thành",
   "address": "Số 81 Chi Lăng, Phường  Nguyễn TrãI, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9441747,
   "Latitude": 106.3279935
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Trung Thịnh",
   "address": "Số 5B Đồng Xuân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.937782,
   "Latitude": 106.3301911
 },
 {
   "STT": 41,
   "Name": "Nhà thuốc Ngọc Hải",
   "address": "Số 225 Nguyễn Lương Bằng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9367262,
   "Latitude": 106.3074928
 },
 {
   "STT": 42,
   "Name": "Nhà thuốc Việt – Pháp",
   "address": "Số 9 Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.936599,
   "Latitude": 106.3295281
 },
 {
   "STT": 43,
   "Name": "Nhà thuốc Trung tâm phòng chống bệnh xã hội",
   "address": "Trung tâm PCBXH ,  Số 150 Quang Trung, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9464172,
   "Latitude": 106.3351185
 },
 {
   "STT": 44,
   "Name": "Nhà thuốc Kim Dung ",
   "address": "Số 13 Chi Lăng, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9463335,
   "Latitude": 106.3281877
 },
 {
   "STT": 45,
   "Name": "Nhà thuốc Tùng Vân",
   "address": "Số 20B Hoàng Diệu, Phường  Cẩm Thượng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9505766,
   "Latitude": 106.3211907
 },
 {
   "STT": 46,
   "Name": "Nhà thuốc Tiến Hà ",
   "address": "Số 24 Tự Đông, Phường  Cẩm Thượng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9498808,
   "Latitude": 106.3173952
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Hoàng Minh ",
   "address": "Cổng BV YHCT, Đường Thanh Niên, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9232884,
   "Latitude": 106.3309164
 },
 {
   "STT": 48,
   "Name": "Nhà thuốc Huy Quốc ",
   "address": "Số 67 Nguyễn Thị Duệ, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9388666,
   "Latitude": 106.3105641
 },
 {
   "STT": 49,
   "Name": "Nhà thuốc 64B Quang Trung",
   "address": "Số 64B Quang Trung, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9413964,
   "Latitude": 106.3340744
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Minh Châu",
   "address": "Ki ốt số 6, chợ Phú Lương, Nguyễn Hữu Cầu, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.940103,
   "Latitude": 106.3480478
 },
 {
   "STT": 51,
   "Name": "Nhà thuốc Lan Anh ",
   "address": "Số 13 Trần Cảnh, Phường  Cẩm Thượng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9501129,
   "Latitude": 106.3206947
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Vĩnh Thịnh",
   "address": "Số 17H Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9369967,
   "Latitude": 106.3298785
 },
 {
   "STT": 53,
   "Name": "Nhà thuốc Hà Trang",
   "address": "Số 36 Tuy Hòa, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9363459,
   "Latitude": 106.3292284
 },
 {
   "STT": 54,
   "Name": "Nhà thuốc Hải Anh",
   "address": "Số 112 Khúc Thừa Dụ, khu 1 Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9335958,
   "Latitude": 106.2981448
 },
 {
   "STT": 55,
   "Name": "Nhà thuốc Hà Nội",
   "address": "Số 19 Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9370147,
   "Latitude": 106.3298546
 },
 {
   "STT": 56,
   "Name": "Nhà thuốc 76",
   "address": "  Số 76 Phạm Hồng Thái, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9397826,
   "Latitude": 106.3345698
 },
 {
   "STT": 57,
   "Name": "Nhà thuốc Thuần Lan",
   "address": "487 Điện Biên Phủ, Phường  Bình Hàn, Thành phố Hải Dương Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9499877,
   "Latitude": 106.3239348
 },
 {
   "STT": 58,
   "Name": "Nhà thuốc Sơn Hải",
   "address": "Số 2 Vũ Hựu, khu 3 Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9386701,
   "Latitude": 106.3070047
 },
 {
   "STT": 59,
   "Name": "Nhà thuốc Khánh Phương",
   "address": "Khu 5 Đồng Niên, Phường  Việt Hòa, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9428052,
   "Latitude": 106.3023909
 },
 {
   "STT": 60,
   "Name": "Nhà thuốc Minh Thủy",
   "address": "Số 10A Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9366688,
   "Latitude": 106.3297696
 },
 {
   "STT": 61,
   "Name": "Nhà thuốc Bình Lộc ",
   "address": "Số 151 Bình Lộc, Phường  Tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9263099,
   "Latitude": 106.3191667
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Dược Khoa",
   "address": "Ki ốt BVĐK Tỉnh số 225 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9353833,
   "Latitude": 106.3071555
 },
 {
   "STT": 63,
   "Name": "Nhà thuốc Ngọc Châu",
   "address": "Ki ốt số 3 chợ Phú Lương, Phường  Ngọc Châu, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.940103,
   "Latitude": 106.3480478
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc Văn Hiến",
   "address": "Số 193 Trương Mỹ, Phường  Phạm Ngũ Lão, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9339702,
   "Latitude": 106.3224324
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc Tân Bình",
   "address": "Số 54 Nguyễn Chí Thanh, Phường  Tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9242401,
   "Latitude": 106.3146275
 },
 {
   "STT": 66,
   "Name": "Nhà thuốc Hoàng Anh",
   "address": "Số 120 Đức Minh, khu 5, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9311013,
   "Latitude": 106.3107065
 },
 {
   "STT": 67,
   "Name": "Nhà thuốc Thúy Nhi ",
   "address": "959 Lê Thanh Nghị, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9212095,
   "Latitude": 106.3228197
 },
 {
   "STT": 68,
   "Name": "Nhà thuốc Nga Oánh ",
   "address": "17I Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9369967,
   "Latitude": 106.3298785
 },
 {
   "STT": 69,
   "Name": "Nhà thuốc Nhật Anh ",
   "address": "26 Nguyễn Thị Duệ, Phường  Phạm Ngũ Lão, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9377883,
   "Latitude": 106.3131737
 },
 {
   "STT": 70,
   "Name": "Nhà thuốc Minh Huyền ",
   "address": "17 Nhà Thờ, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9392,
   "Latitude": 106.3340735
 },
 {
   "STT": 71,
   "Name": "Nhà thuốc BVĐK Hòa Bình",
   "address": "BVĐK Hòa Bình, phố Phạm Xuân Huân, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.930571,
   "Latitude": 106.3401434
 },
 {
   "STT": 72,
   "Name": "nhà thuốc Hồng Hà",
   "address": "số 300 đại lộ trần Hưng Đạo, Phường  Ngọc Châu, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9394286,
   "Latitude": 106.3302706
 },
 {
   "STT": 73,
   "Name": "Nhà thuốc BVYHCT",
   "address": "Đường Thanh Niên, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9249044,
   "Latitude": 106.3315375
 },
 {
   "STT": 74,
   "Name": "Nhà thuốc Tâm Dược ",
   "address": "lô 72.45 khu đô thị phía tây, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.8679737,
   "Latitude": 106.2946196
 },
 {
   "STT": 75,
   "Name": "Nhà thuốc PKĐK Y cao Hà nội ",
   "address": "Km51+900 quốc lộ 5, Phường  Cẩm Thượng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.8502461,
   "Latitude": 106.6532284
 },
 {
   "STT": 76,
   "Name": "Nhà thuốc Duy Hoàng",
   "address": "Lô 23 khu dân cư đông Ngô Quyền, Phường  Tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9242401,
   "Latitude": 106.3146275
 },
 {
   "STT": 77,
   "Name": "Nhà thuốc Á Âu ",
   "address": "36 Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9371469,
   "Latitude": 106.3301121
 },
 {
   "STT": 78,
   "Name": "Nhà thuốc Minh Đức ",
   "address": "Ki ốt số 5 chợ Đông Ngô Quyền, Phường  Tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.93409,
   "Latitude": 106.3173
 },
 {
   "STT": 79,
   "Name": "Nhà thuốc BV điều dưỡng & PHCN",
   "address": "BV Điều dưỡng& PHCN, 112 Đại lộ Trần Hưng Đạo, Phường  Trần Hưng Đạo, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9380802,
   "Latitude": 106.3393836
 },
 {
   "STT": 80,
   "Name": "Nhà thuốc Nhân Dân ",
   "address": "Lô 72.44 khu đô thị phía Tây, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.8679737,
   "Latitude": 106.2946196
 },
 {
   "STT": 81,
   "Name": "Nhà thuốc 74 Trần Hưng Đạo",
   "address": "số 74 Trần Hưng Đạo, Phường  Trần Hưng Đạo, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.940694,
   "Latitude": 106.3428153
 },
 {
   "STT": 82,
   "Name": "nhà thuốc Hà Nguyệt ",
   "address": "Ki ốt số 1 Bệnh viện đa khoa Tỉnh HD, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9408111,
   "Latitude": 106.3276707
 },
 {
   "STT": 83,
   "Name": "Nhà thuốc Phúc Lộc ",
   "address": "số 76 Bình Lộc, khu 10, Phường  Tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9242401,
   "Latitude": 106.3146275
 },
 {
   "STT": 84,
   "Name": "nhà thuốc PKĐK Thanh Bình",
   "address": "lô 02.01, 02 Phường  Tân Bình,, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9242401,
   "Latitude": 106.3146275
 },
 {
   "STT": 85,
   "Name": "nhà thuốc Hương Tâm",
   "address": "số 58A Nguyễn Thượng Mẫn, Phường  Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9463619,
   "Latitude": 106.3246715
 },
 {
   "STT": 86,
   "Name": "Nhà thuốc BVĐK tỉnh HD",
   "address": "Bệnh viện đa khoa Tỉnh, số 225 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9353833,
   "Latitude": 106.3071555
 },
 {
   "STT": 87,
   "Name": "Nhà thuốc Hải Đăng",
   "address": "66B Phạm Hồng Thái, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.939788,
   "Latitude": 106.3342292
 },
 {
   "STT": 88,
   "Name": "Nhà thuốc số 8 Quang Trung",
   "address": "Số 8 Quang Trung, Phường  Quang Trung, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9392033,
   "Latitude": 106.3335353
 },
 {
   "STT": 89,
   "Name": "Nhà thuốc BV Nhi",
   "address": "Số 225 Nguyễn Lương Bằng, Phường   Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9367262,
   "Latitude": 106.3074928
 },
 {
   "STT": 90,
   "Name": "Nhà thuốc Đại Dương",
   "address": "Số 9E Hoàng Văn Thụ, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9370323,
   "Latitude": 106.3304287
 },
 {
   "STT": 91,
   "Name": "Nhà thuốc Tâm Phúc",
   "address": "Số 328 Phố Bình Lộc, Phường  Tân Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9242401,
   "Latitude": 106.3146275
 },
 {
   "STT": 92,
   "Name": "Quầy thuốc số 32B hiệu thuốc TPHD",
   "address": "Số 407 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9364689,
   "Latitude": 106.2972601
 },
 {
   "STT": 93,
   "Name": "Quầy thuốc Hoapharco số 4",
   "address": "Thôn Tiền Trung, xã Ái Quốc, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9663848,
   "Latitude": 106.3763237
 },
 {
   "STT": 94,
   "Name": "Quầy thuốc số 10 ",
   "address": "Số 16 Tô Ngọc Vân, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.930587,
   "Latitude": 106.339962
 },
 {
   "STT": 95,
   "Name": "Quầy thuốc số 28",
   "address": "Số 10 Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9366688,
   "Latitude": 106.3297696
 },
 {
   "STT": 96,
   "Name": "Quầy thuốc số 14 ",
   "address": "Số 264 Điện Biên Phủ, Phường  Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9432016,
   "Latitude": 106.3213515
 },
 {
   "STT": 97,
   "Name": "Quầy thuốc số 22 ",
   "address": "Số 14 Trương Mỹ, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.936659,
   "Latitude": 106.3268231
 },
 {
   "STT": 98,
   "Name": "Quầy thuốc số 19 ",
   "address": "Số 165 Nguyễn Thị Duệ, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9397873,
   "Latitude": 106.3085464
 },
 {
   "STT": 99,
   "Name": "Quầy thuốc số 09 ",
   "address": "Số 12B Phạm Ngũ Lão, Phường  Phạm Ngũ Lão, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.937119,
   "Latitude": 106.326463
 },
 {
   "STT": 100,
   "Name": "Quầy thuốc số 17 ",
   "address": "Chợ Thanh Bình, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9321454,
   "Latitude": 106.2934608
 },
 {
   "STT": 101,
   "Name": "Quầy thuốc số 12 ",
   "address": "Số 418 Điện Biên Phủ, Phường  Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9479575,
   "Latitude": 106.3229703
 },
 {
   "STT": 102,
   "Name": "Quầy thuốc Nam Anh",
   "address": "Thôn Trần Nội, xã Thạch Khôi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9072223,
   "Latitude": 106.3073133
 },
 {
   "STT": 103,
   "Name": "Quầy thuốc Thu Hà",
   "address": "Khu 2, phố Mới, xã Thạch Khôi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.90475,
   "Latitude": 106.3129
 },
 {
   "STT": 104,
   "Name": "Quầy số 02 ",
   "address": "Số 123 Phan Đình Phùng, Phường  Cẩm Thượng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9498429,
   "Latitude": 106.320156
 },
 {
   "STT": 105,
   "Name": "Quầy số 41 ",
   "address": "Số 08 Lý Thường Kiệt, Phường  Trần Phú, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9366675,
   "Latitude": 106.3297686
 },
 {
   "STT": 106,
   "Name": "Quầy số 02  ",
   "address": "Số 267 Điện Biên Phủ, Phường  Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9436081,
   "Latitude": 106.321725
 },
 {
   "STT": 107,
   "Name": "Quầy số 38",
   "address": "Số 136B Thống Nhất, Phường  Lê Thanh Nghị, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9307649,
   "Latitude": 106.3232967
 },
 {
   "STT": 108,
   "Name": "Quầy số 48 ",
   "address": "Số 116 Phan Đình Phùng, Phường  Cẩm Thượng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9503291,
   "Latitude": 106.3193205
 },
 {
   "STT": 109,
   "Name": "Quầy  số 02",
   "address": "Ki ốt 02 số 225 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương, Tỉnh Hải Dương",
   "Longtitude": 20.9367262,
   "Latitude": 106.3074928
 },
 {
   "STT": 110,
   "Name": "Quầy số 18 ",
   "address": "Ki ốt 04 số 225 Nguyễn Lương Bằng, Phường  Thanh Bình, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9367262,
   "Latitude": 106.3074928
 },
 {
   "STT": 111,
   "Name": "Quầy thuốc số 46",
   "address": "Số 103 Yết Kiêu, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9178063,
   "Latitude": 106.3341486
 },
 {
   "STT": 112,
   "Name": "Quầy thuốc số 27",
   "address": "Số 187 Đinh Văn Tả, Phường  Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9511329,
   "Latitude": 106.3323482
 },
 {
   "STT": 113,
   "Name": "Quầy thuốc số 06",
   "address": "Số 32 phố Ga, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.94741,
   "Latitude": 106.3284
 },
 {
   "STT": 114,
   "Name": "Quầy thuốc Hoàn Trang",
   "address": "Thôn Tiền Trung, xã Ái Quốc, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9663848,
   "Latitude": 106.3763237
 },
 {
   "STT": 115,
   "Name": "Quầy thuốc số 15",
   "address": "Thôn Tiền, xã An Châu, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9376419,
   "Latitude": 106.3170004
 },
 {
   "STT": 116,
   "Name": "Quầy thuốc số 04",
   "address": "Thôn Tiền Trung, xã Aí Quốc, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9663848,
   "Latitude": 106.3763237
 },
 {
   "STT": 117,
   "Name": "Quầy thuốc Hoàng Huy",
   "address": "Tiền Trung, xã Ái Quốc, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9583112,
   "Latitude": 106.3823846
 },
 {
   "STT": 118,
   "Name": "Quầy thuốc Trung Nga",
   "address": "khu 5, Phường  Việt Hòa, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9484328,
   "Latitude": 106.2995167
 },
 {
   "STT": 119,
   "Name": "Quầy thuốc số 17",
   "address": "Khu 2, chợ Hui, xã Thạch Khôi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9060854,
   "Latitude": 106.3112104
 },
 {
   "STT": 120,
   "Name": "Quầy số 15",
   "address": "Số 2 Mai Hắc Đế, khu17 Bình Hàn, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9421381,
   "Latitude": 106.3207826
 },
 {
   "STT": 121,
   "Name": "Quầy số 08",
   "address": "Số 155 Chi Lăng, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9423355,
   "Latitude": 106.3277881
 },
 {
   "STT": 122,
   "Name": "Quầy số 26",
   "address": "Số 19 Yết Kiêu, Phường  Hải Tân, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9214868,
   "Latitude": 106.3309075
 },
 {
   "STT": 123,
   "Name": "Quầy số 11",
   "address": "Số 118 Lê Hồng Phong, Phường  Nguyễn Trãi, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9382868,
   "Latitude": 106.3255803
 },
 {
   "STT": 124,
   "Name": "Chi nhánh CTCP Dược VTYT Hải Dương tại Chí Linh",
   "address": "Số 119 Nguyễn Trãi, Phường  Sao Đỏ, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1122533,
   "Latitude": 106.3915553
 },
 {
   "STT": 125,
   "Name": "Nhà thuốc bệnh viên",
   "address": "Bệnh viện đa khoa Chí Linh, Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1101301,
   "Latitude": 106.3866208
 },
 {
   "STT": 126,
   "Name": "Quầy thuốc Hoapharco số 6",
   "address": "43 Nguyễn Thái Học I, Thị trấnr Sao Đỏ, Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1093403,
   "Latitude": 106.3938291
 },
 {
   "STT": 127,
   "Name": "Quầy thuốc  PKĐK 71 Hùng Vương",
   "address": "Số 71 Hùng Vương, Thị trấn Sao Đỏ, Huyện Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1129117,
   "Latitude": 106.3967765
 },
 {
   "STT": 128,
   "Name": "Quầy thuốc số 1 công ty dược ánh Dương",
   "address": "Số 107 Thái học 1, Thị trấnrấn Sao Đỏ, Huyện Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1128901,
   "Latitude": 106.3931932
 },
 {
   "STT": 129,
   "Name": "Quầy thuốc Hoapharco số 5",
   "address": "Số 13 Thái Học 1, Thị trấnrấn Sao Đỏ, Huyện Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1126342,
   "Latitude": 106.3929628
 },
 {
   "STT": 130,
   "Name": "Quầy thuốc ánh Điệp",
   "address": "Số 57B Trần Hưng Đạo,  Phường  Phả Lại, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1131025,
   "Latitude": 106.3234111
 },
 {
   "STT": 131,
   "Name": "Quầy thuốc ánh Nguyệt",
   "address": "Số 428 Trần Hưng Đạo, Phường  Sao Đỏ, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1087517,
   "Latitude": 106.3874175
 },
 {
   "STT": 132,
   "Name": "Quầy thuốc Hương Cường",
   "address": "Thôn Trại Sen, Phường  Văn An, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.0970404,
   "Latitude": 106.351537
 },
 {
   "STT": 133,
   "Name": "Quầy thuốc số 01- HT Chí Linh",
   "address": "Cổng BVĐK Chí Linh, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1101301,
   "Latitude": 106.3866208
 },
 {
   "STT": 134,
   "Name": "Quầy thuốc số 15- HT Chí Linh",
   "address": "Thôn Lôi Động, Phường  Cộng Hòa, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1348523,
   "Latitude": 106.3875898
 },
 {
   "STT": 135,
   "Name": "Quầy thuốc số 5 chi nhánh Chí Linh",
   "address": "Số 114 Hùng Vương, Phường  Sao Đỏ, Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1085338,
   "Latitude": 106.3905338
 },
 {
   "STT": 136,
   "Name": "Quầy thuốc số 17 chi nhánh Chí Linh",
   "address": "Số 224 Nguyễn Trãi II, Phường  Sao Đỏ,Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1130318,
   "Latitude": 106.3969466
 },
 {
   "STT": 137,
   "Name": "Quầy thuốc  Vĩnh Thiên",
   "address": "Phố Thiên, Phường  Thái Học,Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.161335,
   "Latitude": 106.416838
 },
 {
   "STT": 138,
   "Name": "Quầy thuốc số 06 chi nhánh Chí Linh",
   "address": "Số 376 Lục Đầu Giang, Phường  Phả Lại,Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1141829,
   "Latitude": 106.3010847
 },
 {
   "STT": 139,
   "Name": "Quầy thuốc Nguyễn Văn Hợp",
   "address": "khu dân cư Đại Tân, Phường  Hoàng Tân, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1369114,
   "Latitude": 106.4324895
 },
 {
   "STT": 140,
   "Name": "Quầy thuốc Nguyễn Thị Vân",
   "address": "khu dân cư, Phường  Bến Tắm, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1741082,
   "Latitude": 106.458256
 },
 {
   "STT": 141,
   "Name": "Quầy thuốc Nguyễn Thị Hạnh",
   "address": "khu dân cư, Phường  Bến Tắm, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1741082,
   "Latitude": 106.458256
 },
 {
   "STT": 142,
   "Name": "Quầy thuốc Thềm Xuyến",
   "address": "số 2 Nguyễn Trãi 1, Phường  Sao Đỏ, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1129674,
   "Latitude": 106.3919502
 },
 {
   "STT": 143,
   "Name": "Quầy thuốc GPP trung tâm hiệu thuốc Chí Linh",
   "address": "Số 119 Nguyễn Trãi I, Thị trấnrấn Sao Đỏ, Thị xã Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1122874,
   "Latitude": 106.3915955
 },
 {
   "STT": 144,
   "Name": "Quầy thuốc số 11",
   "address": "529 Nguyễn Trãi II, Phường  Sao Đỏ, Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1130318,
   "Latitude": 106.3969466
 },
 {
   "STT": 145,
   "Name": "Quầy thuốc số 15 ",
   "address": "Thôn Lôi Động, Phường  Cộng Hòa, Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1348523,
   "Latitude": 106.3875898
 },
 {
   "STT": 146,
   "Name": "Quầy thuốc số 16",
   "address": "Số 77 Trần Hưng Đạo, Phường  Phả Lại, Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.1134554,
   "Latitude": 106.3234268
 },
 {
   "STT": 147,
   "Name": "Quầy thuốc số  13",
   "address": "Thôn Trung Tâm, xã Hoàng Tiến, Thị xã  Chí Linh,Tỉnh Hải Dương",
   "Longtitude": 21.129768,
   "Latitude": 106.450473
 },
 {
   "STT": 148,
   "Name": "Chi nhánh CTCP Dược VTYT Hải Dương tại Thanh Miện",
   "address": "Khu 4, Thị trấn Thanh Miện, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7882863,
   "Latitude": 106.2378962
 },
 {
   "STT": 149,
   "Name": "Nhà thuốc Yến Trường",
   "address": "Số 104 khu I, Thị trấn Thanh Miện, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7879784,
   "Latitude": 106.237505
 },
 {
   "STT": 150,
   "Name": "Quầy thuốc HT Thanh Miện",
   "address": "Phạm Lâm, Đoàn Tùng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8280271,
   "Latitude": 106.2272092
 },
 {
   "STT": 151,
   "Name": "Quầy thuốc – HT Thanh Miện",
   "address": "Chợ Bóng, Quang Minh, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.7913991,
   "Latitude": 106.2732035
 },
 {
   "STT": 152,
   "Name": "Quầy thuốc của HT Thanh Miện",
   "address": "Số 8, khu 3, Thị trấn Thanh Miện, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7855735,
   "Latitude": 106.2413274
 },
 {
   "STT": 153,
   "Name": "Quầy thuốc Tuyên Lan",
   "address": "Thôn Đoàn, xã Thanh Tùng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8243987,
   "Latitude": 106.2198553
 },
 {
   "STT": 154,
   "Name": "Quầy thuốc Trường Hải",
   "address": "Số 86 Lê Bình, Thị trấnrấn Thanh Miện, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7821204,
   "Latitude": 106.2203815
 },
 {
   "STT": 155,
   "Name": "Quầy thuốc Kim Trang",
   "address": "Thôn Hoà Bình, xã Cao Thắng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.6834752,
   "Latitude": 105.6588485
 },
 {
   "STT": 156,
   "Name": "Quầy thuốc trung tâm HT Thanh Miện",
   "address": "Khu 4, Thị trấn Thanh Miện, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7882863,
   "Latitude": 106.2378962
 },
 {
   "STT": 157,
   "Name": "Quầy thuốc bệnh viện Thanh Miện",
   "address": "Khu 4, Thị trấn Thanh Miện, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7882863,
   "Latitude": 106.2378962
 },
 {
   "STT": 158,
   "Name": "Quầy thuốc Thanh Long Na",
   "address": "Đào Lâm, xã Đoàn Tùng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.825703,
   "Latitude": 106.2213261
 },
 {
   "STT": 159,
   "Name": "Quầy thuốc Lê Hồng",
   "address": "Hoành Bồ, xã Lê Hồng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7730057,
   "Latitude": 106.1977954
 },
 {
   "STT": 160,
   "Name": "Quầy thuốc số 2",
   "address": "thị tứ Lam Sơn, xã Lam Sơn, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8035955,
   "Latitude": 106.2143401
 },
 {
   "STT": 161,
   "Name": "Đại lý bán thuốc cho  HT Thanh Miện",
   "address": "Từ Ô, Tân Trào, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7901828,
   "Latitude": 106.1470668
 },
 {
   "STT": 162,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Đào Lâm, Đoàn Tùng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.825703,
   "Latitude": 106.2213261
 },
 {
   "STT": 163,
   "Name": "Đại lý bán thuốc cho  HT Thanh Miện",
   "address": "La Ngoại, Ngũ Hùng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7551065,
   "Latitude": 106.2558918
 },
 {
   "STT": 164,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Chợ Thông, xã Đoàn Tùng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8238772,
   "Latitude": 106.2197266
 },
 {
   "STT": 165,
   "Name": "Đại lý bán thuốc cho HT TM",
   "address": "An Khoái, Tứ Cường, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.765573,
   "Latitude": 106.2448595
 },
 {
   "STT": 166,
   "Name": "Đại lý bán thuốc cho HT TM",
   "address": "Phương Khê, Chi Lăng Bắc, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.738293,
   "Latitude": 106.2352987
 },
 {
   "STT": 167,
   "Name": "Đại lý bán thuốc cho  HT Thanh Miện",
   "address": "K3, Thị trấn Thanh Miện, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7861757,
   "Latitude": 106.2414397
 },
 {
   "STT": 168,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Chợ Chùa, thôn Đông, Đoàn Tùng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.831349,
   "Latitude": 106.2262581
 },
 {
   "STT": 169,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Thôn My Động, Tiền Phong, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7048993,
   "Latitude": 106.2507963
 },
 {
   "STT": 170,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Hoành Bồ, Lê Hồng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7730057,
   "Latitude": 106.1977954
 },
 {
   "STT": 171,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Khu 3 thị tứ Đoàn Tùng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8243987,
   "Latitude": 106.2198553
 },
 {
   "STT": 172,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Chợ Neo, Thị trấn Thanh Miện, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7867469,
   "Latitude": 106.2447573
 },
 {
   "STT": 173,
   "Name": "Đại lý bán thuốc cho HT TM",
   "address": "An Bình, Hồng Quang, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8102757,
   "Latitude": 106.208825
 },
 {
   "STT": 174,
   "Name": "Đại lý bán thuốc cho  HT TM",
   "address": " khu 3, Thị trấn Thanh Miện, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7883796,
   "Latitude": 106.2365609
 },
 {
   "STT": 175,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Thị tứ Thanh Giang, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7235746,
   "Latitude": 106.251772
 },
 {
   "STT": 176,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Phương Quan, Lê Hồng, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7781259,
   "Latitude": 106.2084574
 },
 {
   "STT": 177,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Đông La, Hồng Quang, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8062481,
   "Latitude": 106.1830901
 },
 {
   "STT": 178,
   "Name": "Đại lý bán thuốc cho HT Thanh Miện",
   "address": "Khu xóm Chợ, Thanh Giang, Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7235746,
   "Latitude": 106.251772
 },
 {
   "STT": 179,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn An Lâu, xã Hồng Quang, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8089806,
   "Latitude": 106.1950745
 },
 {
   "STT": 180,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Đỗ Thượng, xã Phạm Kha, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8284774,
   "Latitude": 106.237505
 },
 {
   "STT": 181,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Đông La, xã Hồng Quang, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8064086,
   "Latitude": 106.1837767
 },
 {
   "STT": 182,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Từ Xá, xã Đoàn Kết, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7883865,
   "Latitude": 106.1658474
 },
 {
   "STT": 183,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Tiên Động, xã Tiền Phong, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7046036,
   "Latitude": 106.2584661
 },
 {
   "STT": 184,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Hoành Bồ, xã Lê Hồng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7730057,
   "Latitude": 106.1977954
 },
 {
   "STT": 185,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Phương Quan, xã Lê Hồng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7781259,
   "Latitude": 106.2084574
 },
 {
   "STT": 186,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Gia Cốc, xã Tứ Cường, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7726009,
   "Latitude": 106.235282
 },
 {
   "STT": 187,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Châu Quan, xã Đoàn Kết, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7885565,
   "Latitude": 106.1595053
 },
 {
   "STT": 188,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Hội Yên, xã Chi Lăng Nam, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.732366,
   "Latitude": 106.2367696
 },
 {
   "STT": 189,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thị tứ Thanh Giang,Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7235746,
   "Latitude": 106.251772
 },
 {
   "STT": 190,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Thôn Tòng Hoá, xã Đoàn Kết, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7765177,
   "Latitude": 106.1785554
 },
 {
   "STT": 191,
   "Name": "Đại lý số  32 hiệu thuốc Thanh Miện",
   "address": "Thôn Thuý Lâm, xã Đoàn Tùng, Huyện Thanh Miện ,Tỉnh Hải Dương",
   "Longtitude": 20.8147833,
   "Latitude": 106.2220614
 },
 {
   "STT": 192,
   "Name": "Đại lý số  31 hiệu thuốc Thanh Miện",
   "address": "Phố Chương, xã Lam Sơn, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8074924,
   "Latitude": 106.2301508
 },
 {
   "STT": 193,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Thanh Miện",
   "address": "Thôn Quốc Tuấn, xã Lê Hồng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7829899,
   "Latitude": 106.199266
 },
 {
   "STT": 194,
   "Name": "Đại lý hiệu thuốc Thanh Miện",
   "address": "Thôn Thọ Trương, xã Lam Sơn, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8038636,
   "Latitude": 106.2227968
 },
 {
   "STT": 195,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Thanh Miện",
   "address": "Thôn Hội Yên, xã Chi Lăng Nam, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.732366,
   "Latitude": 106.2367696
 },
 {
   "STT": 196,
   "Name": "Chi nhánh CTCP Dược VTYT Hải Dương tại Tứ Kỳ",
   "address": "Thị trấnr Tứ Kỳ, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.817652,
   "Latitude": 106.4081983
 },
 {
   "STT": 197,
   "Name": "Nhà thuốc BVĐK Tứ Kỳ",
   "address": "Thị trấn Tứ Kỳ, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.817652,
   "Latitude": 106.4081983
 },
 {
   "STT": 198,
   "Name": "Quầy thuốc Vân Dũng",
   "address": "Thôn Thanh Kỳ, xã An Thanh, Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8063175,
   "Latitude": 106.4499824
 },
 {
   "STT": 199,
   "Name": "Quầy  thuốc  GPP trung tâm hiệu thuốc Tứ Kỳ",
   "address": "Thị trấn Tứ Kỳ, Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.817652,
   "Latitude": 106.4081983
 },
 {
   "STT": 200,
   "Name": "Quầy thuốc Đức Tín",
   "address": "Thôn Nghĩa Dũng, xã Đại Đồng, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.872406,
   "Latitude": 106.3655949
 },
 {
   "STT": 201,
   "Name": "Quầy thuốc Hoapharco số 8",
   "address": "Thôn La Tỉnh, Thị trấnrấn Tứ Kỳ, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.817652,
   "Latitude": 106.408842
 },
 {
   "STT": 202,
   "Name": "Quầy thuốc Quỳnh Tâm",
   "address": "Thôn Bỉnh Dy, xã Kỳ Sơn, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8813478,
   "Latitude": 106.362505
 },
 {
   "STT": 203,
   "Name": "Quầy thuốc Nga Linh",
   "address": "Thôn Thanh Kỳ, xã An Thanh, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8040766,
   "Latitude": 106.458256
 },
 {
   "STT": 204,
   "Name": "Quầy thuốc Đào Thị Thuẫn",
   "address": "Thôn Hàm Cách, xã Hà THanh, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.7396152,
   "Latitude": 106.4376425
 },
 {
   "STT": 205,
   "Name": "Quầy thuốc Hương Lâm",
   "address": "Thôn Cầu Xe, xã Quang Trung, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.803485,
   "Latitude": 106.4052541
 },
 {
   "STT": 206,
   "Name": "Quầy thuốc bán thuốc cho CN Công ty CPDược VTYT",
   "address": "Thôn Lạc Dục, xã Hưng Đạo, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8573781,
   "Latitude": 106.3755942
 },
 {
   "STT": 207,
   "Name": "Quầy thuốc Nguyễn Thị Huế",
   "address": "Khu An Nhân Tây, Thị trấnr Tứ Kỳ, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8220925,
   "Latitude": 106.3972746
 },
 {
   "STT": 208,
   "Name": "Quầy thuốc PKĐK Hưng Đạo",
   "address": "thị tứ xã Hưng Đạo, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8570573,
   "Latitude": 106.3754225
 },
 {
   "STT": 209,
   "Name": "Quầy thuốc số 02",
   "address": "Thôn An Lại, xã Dân Chủ, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8260594,
   "Latitude": 106.320818
 },
 {
   "STT": 210,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Thôn Đại Hà, xã Hà Kỳ, Huyện Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.7486865,
   "Latitude": 106.4222797
 },
 {
   "STT": 211,
   "Name": "Đại lý Hoaphaco số 7",
   "address": "Thôn La Tỉnh, Thị trấnrấn Tứ Kỳ, Huyện Tứ kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.817652,
   "Latitude": 106.408842
 },
 {
   "STT": 212,
   "Name": "Đại lý Hoapharco số 5",
   "address": "Thôn Thanh Kỳ, xã An Thanh, Tứ Kỳ,Tỉnh Hải Dương",
   "Longtitude": 20.8063175,
   "Latitude": 106.4499824
 },
 {
   "STT": 213,
   "Name": "Chi nhánh công ty CPDược &VTYT tại Kim Thành ",
   "address": "Phố Ga, Thị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9630478,
   "Latitude": 106.5150108
 },
 {
   "STT": 214,
   "Name": "CTCP Dược phẩm Hòa Hưng",
   "address": "Thôn Thanh Liên, xã Cộng Hòa, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9783991,
   "Latitude": 106.4199755
 },
 {
   "STT": 215,
   "Name": "Quầy thuốc Minh Dương",
   "address": "Thị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 216,
   "Name": "Quầy thuốc Phong Thủy",
   "address": "Thôn Thanh Liên, xã Cộng Hòa, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9783991,
   "Latitude": 106.4199755
 },
 {
   "STT": 217,
   "Name": "Quầy thuốc Thành Sơn",
   "address": "Thôn Quyết Thắng, xã Kim Anh,Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9515873,
   "Latitude": 106.512022
 },
 {
   "STT": 218,
   "Name": "Quầy thuốc Hoapharco số 2",
   "address": "Thôn Tân Phú, Thị trấnrấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 219,
   "Name": "Quầy thuốc Hoapharco số 3",
   "address": "Thôn Văn Minh, xã Kim Anh, Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9545609,
   "Latitude": 106.5063857
 },
 {
   "STT": 220,
   "Name": "Quầy thuốc số 7 hiệu  thuốc Kim Thành ",
   "address": "Ngã tư thị tứ Kim Đính, Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9141947,
   "Latitude": 106.4847619
 },
 {
   "STT": 221,
   "Name": "Quầy thuốc Minh Nguyệt",
   "address": "Thôn Cống Khê, xã Kim Khê, Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9381099,
   "Latitude": 106.5300957
 },
 {
   "STT": 222,
   "Name": "Quầy thuốc  Bùi Lệ",
   "address": "Thôn Hợp Nhất, xã Lai Vu, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9810483,
   "Latitude": 106.4064615
 },
 {
   "STT": 223,
   "Name": "Quầy thuốc của phòng khám đa khoa",
   "address": "Thôn Quyết Thắng, xã Kim Anh, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9515873,
   "Latitude": 106.512022
 },
 {
   "STT": 224,
   "Name": "Quầy thuốc số 1  hiệu thuốc  Kim Thành",
   "address": "Phố GaThị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9630478,
   "Latitude": 106.5150108
 },
 {
   "STT": 225,
   "Name": "Quầy thuốc số 5 hiệu thuốc Kim Thành",
   "address": "Thôn Viên Chử, xã Kim Tân, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9188692,
   "Latitude": 106.5178991
 },
 {
   "STT": 226,
   "Name": "Quầy thuốc số 2  hiệu thuốc  Kim Thành",
   "address": "Khu Đồng Văn, Thị trấn Phú Thái. Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9634686,
   "Latitude": 106.5149035
 },
 {
   "STT": 227,
   "Name": "Quầy thuốc số 4 HT  Kim Thành",
   "address": "Thị tứ Đồng Gia, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.8949674,
   "Latitude": 106.51636
 },
 {
   "STT": 228,
   "Name": "Quầy thuốc BVĐK Kim Thành",
   "address": "Thôn Tân Phú, Thị trấn  Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 229,
   "Name": "Quầy thuốc Điện Biên Phủ",
   "address": "Thôn Tân Phú, Thị trấn  Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 230,
   "Name": "Quầy thuốc Hoapharco số 16",
   "address": "Thôn Tân Phú, Thị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 231,
   "Name": "Quầy  số 27-HT Kim Thành",
   "address": "Xóm Mới, xã Cẩm La, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9085344,
   "Latitude": 106.5191845
 },
 {
   "STT": 232,
   "Name": "Quầy số 18-HT Kim Thành",
   "address": "Thôn An Thái, Thị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9668913,
   "Latitude": 106.5111557
 },
 {
   "STT": 233,
   "Name": "Quầy thuốc Tuấn Dung",
   "address": "Thôn Thanh Liên, xã Cộng Hòa, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9783991,
   "Latitude": 106.4199755
 },
 {
   "STT": 234,
   "Name": "Quầy số 9- HT Kim Thành",
   "address": "Thôn Bằng lai, xã Ngũ Phúc, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9372379,
   "Latitude": 106.495051
 },
 {
   "STT": 235,
   "Name": "Quầy thuốc hoapharco số 25",
   "address": "Thôn Lương Xá, xã Kim Lương, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9537505,
   "Latitude": 106.5269084
 },
 {
   "STT": 236,
   "Name": "Quầy thuốc Hoapharco số 01",
   "address": "Thôn Thanh Liên, xã Cộng Hòa, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9783991,
   "Latitude": 106.4199755
 },
 {
   "STT": 237,
   "Name": "Đại lý số 2 Công ty CPDP Hoà Hưng",
   "address": "Thôn Bắc, xã Cổ Dũng, Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.975938,
   "Latitude": 106.4302811
 },
 {
   "STT": 238,
   "Name": "Đại lý hiệu thuốc  Kim Thành",
   "address": "Thôn Đồng Văn, Thị trấnrấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 239,
   "Name": "Đại lý hiệu thuốc Kim Thành",
   "address": "Thôn Quyết Thắng, xã Kim Anh, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9515873,
   "Latitude": 106.512022
 },
 {
   "STT": 240,
   "Name": "Đại lý số 2 hiệu thuốc Kim Thành",
   "address": "Thôn Phạm Xá, xã Tuấn Hưng, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9676912,
   "Latitude": 106.4484215
 },
 {
   "STT": 241,
   "Name": "Đại lý số 1B -  Hiệu thuốc Kim Thành",
   "address": "Thôn Tân Phú, Thị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 242,
   "Name": "Đại lý bán thuốc hiệu thuốc Kim Thành",
   "address": "Đội 6, xã Liên Hoà, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9184944,
   "Latitude": 106.4923316
 },
 {
   "STT": 243,
   "Name": "Đại lý số 16 hiệu thuốc Kim Thành",
   "address": "Thôn Cống Khê, xã Kim Khê, Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9381099,
   "Latitude": 106.5300957
 },
 {
   "STT": 244,
   "Name": "Đại lý số 15 – HT Kim Thành",
   "address": "Xã An Thái, Thị trấn Phú Thái, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9633083,
   "Latitude": 106.5142169
 },
 {
   "STT": 245,
   "Name": "Đại lý bán thuốc cho HT Kim Thành",
   "address": "Khu trung tâm, xã Tuấn Hưng, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9689986,
   "Latitude": 106.452257
 },
 {
   "STT": 246,
   "Name": "Đại lý số 19- HT Kim Thành",
   "address": "Thôn Phương Duệ, xã Kim Xuyên, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9735449,
   "Latitude": 106.464314
 },
 {
   "STT": 247,
   "Name": "Đại  lý bán thuốc cho HT Bình Giang",
   "address": "Thôn Bằng Giã, xã Tân Việt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8863197,
   "Latitude": 106.2147045
 },
 {
   "STT": 248,
   "Name": "Đại lý bán thuốc cho HT Kim Thành",
   "address": "Thôn Bằng Lai, xã Ngũ Phúc, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9372379,
   "Latitude": 106.495051
 },
 {
   "STT": 249,
   "Name": "Đại lý bán thuốc cho HT H.Kim Thành",
   "address": "Thôn Bắc, xã Cổ Dũng, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.977651,
   "Latitude": 106.4315364
 },
 {
   "STT": 250,
   "Name": "Đại lý số 8- HT Kim Thành",
   "address": "Thôn Đông, xã Cổ Dũng, Huyện Kim Thành,Tỉnh Hải Dương",
   "Longtitude": 20.9759557,
   "Latitude": 106.4317455
 },
 {
   "STT": 251,
   "Name": "Nhà thuốc Lý Thăng",
   "address": "Thôn Bung, Thị trấnrấn Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8704652,
   "Latitude": 106.2992146
 },
 {
   "STT": 252,
   "Name": "Nhà thuốc Hải Hà",
   "address": "Quán Nghiên, xã Gia Xuyên, Huyện GIa Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.861619,
   "Latitude": 106.2895489
 },
 {
   "STT": 253,
   "Name": "Nhà thuốc bệnh viện tâm thần",
   "address": "Bệnh viện tâm thần xã Gia Xuyên, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8889237,
   "Latitude": 106.29716
 },
 {
   "STT": 254,
   "Name": "Quầy thuốc Thuyên Tuyển",
   "address": "Thôn Tâng Thượng, xã Liên Hồng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.9069496,
   "Latitude": 106.286284
 },
 {
   "STT": 255,
   "Name": "Quầy thuốc Tẩu Linh",
   "address": "Thôn KênhTriều, xã Thồng Kênh, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8182274,
   "Latitude": 106.3059123
 },
 {
   "STT": 256,
   "Name": "Quầy thuốc Hoaphaco số 22",
   "address": "Thôn Nhân Nghĩa, xã Nam Đồng, Thành phố Hải Dương,Tỉnh Hải Dương",
   "Longtitude": 20.9523823,
   "Latitude": 106.3592434
 },
 {
   "STT": 257,
   "Name": "Quầy thuốc Ngân Tâm",
   "address": "Thôn Tranh Đấu, xã Gia Xuyên, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8685665,
   "Latitude": 106.299414
 },
 {
   "STT": 258,
   "Name": "Quầy thuốc Chi nhánh CT CPDược VTYT tại H.Gia Lộc",
   "address": "Thôn Cao Dương, xã Gia Khánh, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8632415,
   "Latitude": 106.3170905
 },
 {
   "STT": 259,
   "Name": "Quầy  số 14 Chi nhánh CT CPDược VTYT tại H.Gia Lộc",
   "address": "Thôn Cẩm Đới, xã Thống Nhất, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8991898,
   "Latitude": 106.255517
 },
 {
   "STT": 260,
   "Name": "Quầy thuốc số 5",
   "address": "Đường Lê Thanh Nghị, Cầu gỗ, xã Phương Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8662156,
   "Latitude": 106.2925261
 },
 {
   "STT": 261,
   "Name": "Quầy thuốc số 8 ",
   "address": "Thị tứ Quang Minh, xã Quang Minh, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.7894231,
   "Latitude": 106.2709665
 },
 {
   "STT": 262,
   "Name": "Quầy thuốc số 2 ",
   "address": "Số 166 Nguyễn Chế Nghĩa, Thị trấn Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8745302,
   "Latitude": 106.2982646
 },
 {
   "STT": 263,
   "Name": "Quầy thuốc Tuấn Tài",
   "address": "Thôn Minh Tân, xã Quang Minh, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.7899421,
   "Latitude": 106.2708041
 },
 {
   "STT": 264,
   "Name": "Quầy thuốc Đỗ Thị Nhơn",
   "address": "Thị Tứ Hồng Hưng, xã Hồng Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8325629,
   "Latitude": 106.3013717
 },
 {
   "STT": 265,
   "Name": "Quầy thuốc Thanh Long",
   "address": "Quang Bị, xã Phạm Trấn, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8261978,
   "Latitude": 106.2610404
 },
 {
   "STT": 266,
   "Name": "Quầy thuốc Mai Thoảng",
   "address": "thôn lãng Xuyên, xã Gia Tân, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8773381,
   "Latitude": 106.3074017
 },
 {
   "STT": 267,
   "Name": "Quầy thuốc Sơn Phong",
   "address": "Số 05, Đường Lê Thanh Nghị, Thị trấn Gia Lộc,Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8670381,
   "Latitude": 106.2960303
 },
 {
   "STT": 268,
   "Name": "Quầy thuốc số 11",
   "address": "Quán Trắm, xã Hoàng Diệu, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8446373,
   "Latitude": 106.30885
 },
 {
   "STT": 269,
   "Name": "Quầy thuốc số 06",
   "address": "88 Phố Giỗ, Thị trấn Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8716153,
   "Latitude": 106.2960679
 },
 {
   "STT": 270,
   "Name": "Quầy thuốc số 15",
   "address": "Thôn Đĩnh Đào, xã Đoàn Thượng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8154028,
   "Latitude": 106.2812498
 },
 {
   "STT": 271,
   "Name": "Quầy thuốc số 09",
   "address": "Ngã tư chợ Bóng, xã Quang Minh, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.7913991,
   "Latitude": 106.2732035
 },
 {
   "STT": 272,
   "Name": "Quầy thuốc số 04",
   "address": "Thôn Chằm, xã Phương Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.863767,
   "Latitude": 106.2853407
 },
 {
   "STT": 273,
   "Name": "Quầy thuốc số 10",
   "address": "Thị tứ Quán Phe, xã Hồng Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8359296,
   "Latitude": 106.3118833
 },
 {
   "STT": 274,
   "Name": "Quầy thuốc số 13",
   "address": "93 Lê Thanh Nghị, Thị trấn Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.86757,
   "Latitude": 106.297783
 },
 {
   "STT": 275,
   "Name": "Quầy thuốc số 18",
   "address": "Chợ Buộm, xã Yết Kiêu, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8711247,
   "Latitude": 106.2601196
 },
 {
   "STT": 276,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Gia Lộc",
   "address": "Chợ Phe, Hồng Hưng, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8351528,
   "Latitude": 106.3120472
 },
 {
   "STT": 277,
   "Name": "Đại lý số 36 hiệu thuốc Gia Lộc",
   "address": "Thôn Thanh Liễu, xã Tân Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.9024759,
   "Latitude": 106.3243053
 },
 {
   "STT": 278,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Đồng Mỏ, Thị trấnr Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8782566,
   "Latitude": 106.2955346
 },
 {
   "STT": 279,
   "Name": "Đại lý bán thuốc hiệu thuốc Gia Lộc",
   "address": "Thôn Thượng Bì, xã Yết Kiêu, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8684751,
   "Latitude": 106.3022339
 },
 {
   "STT": 280,
   "Name": "Đại lý bán thuốc hiệu thuốc Gia Lộc",
   "address": "Thôn Bái Thượng, xã Toàn Thắng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8428255,
   "Latitude": 106.289764
 },
 {
   "STT": 281,
   "Name": "Đại lý số 39 hiệu thuốc Gia Lộc",
   "address": "Thôn Thị Xá, xã Hồng Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8325629,
   "Latitude": 106.3013717
 },
 {
   "STT": 282,
   "Name": "Đại lý  số 17 hiệu thuốc Gia Lộc",
   "address": "Thôn Thanh Xá, xã Liên Hồng, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.9069496,
   "Latitude": 106.286284
 },
 {
   "STT": 283,
   "Name": "Đại  lý số 42 hiệu thuốc Gia Lộc",
   "address": "Thôn Phương Điếm, Thị trấnrấn Gia Lộc, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8612179,
   "Latitude": 106.2863731
 },
 {
   "STT": 284,
   "Name": "Đại lý công ty TNHH DP Thành Đông",
   "address": "Thôn Nam Cầu, xã Phạm Trấn, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8261978,
   "Latitude": 106.2610404
 },
 {
   "STT": 285,
   "Name": "Đại lý bán thuốc cho  hiệu thuốc Gia Lộc",
   "address": "Thôn Bái Hạ, xã Toàn Thắng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8464817,
   "Latitude": 106.2904635
 },
 {
   "STT": 286,
   "Name": "Đại lý số 16 hiệu thuốc Gia lộc",
   "address": "Thôn Cao Duệ, xã Nhật Tân, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8044763,
   "Latitude": 106.2537162
 },
 {
   "STT": 287,
   "Name": "Đại lý Hoapharco số 10",
   "address": "Thị tứ Đoàn Thượng, xã Đoàn Thượng, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8154028,
   "Latitude": 106.2812498
 },
 {
   "STT": 288,
   "Name": "Đại lý số 43 ",
   "address": "Thôn Thượng Điếm, Thị trấn Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8612295,
   "Latitude": 106.287521
 },
 {
   "STT": 289,
   "Name": "Đại lý số  44 -  Hiệu thuốc Gia Lộc",
   "address": "Thôn Đôn Thư, xã Đồng Quang, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8087576,
   "Latitude": 106.2672429
 },
 {
   "STT": 290,
   "Name": "Đại lý số 14 ",
   "address": "Thôn Tầng Hạ, xã Gia Xuyên, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8904661,
   "Latitude": 106.314005
 },
 {
   "STT": 291,
   "Name": "Đại lý số 23 ",
   "address": "Thôn Thị Xá, xã Hồng Hưng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8325629,
   "Latitude": 106.3013717
 },
 {
   "STT": 292,
   "Name": "Đại lý số 46",
   "address": "Thôn Thanh Xá, xã Liên Hồng, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.9069496,
   "Latitude": 106.286284
 },
 {
   "STT": 293,
   "Name": "Đại lý số 13",
   "address": "Chợ Cuối, Thị trấn Gia Lộc, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8681217,
   "Latitude": 106.3012223
 },
 {
   "STT": 294,
   "Name": "Đại lý số 5",
   "address": "Thôn Trúc Lãm, xã Hoàng Diệu, Huyện Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8420298,
   "Latitude": 106.3213623
 },
 {
   "STT": 295,
   "Name": "Đại lý số  03",
   "address": "Thôn Tranh Đấu, xã Gia Xuyên, Gia Lộc,Tỉnh Hải Dương",
   "Longtitude": 20.8685665,
   "Latitude": 106.299414
 },
 {
   "STT": 296,
   "Name": "Chi nhánh CTCP Dược VTYT Hải Dương tại Cẩm Giàng",
   "address": "Phố Ghẽ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 297,
   "Name": "Quầy thuốc Bình Định",
   "address": "Thôn Tràng Kỹ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9302578,
   "Latitude": 106.2154341
 },
 {
   "STT": 298,
   "Name": "Quầy thuốc An Phú",
   "address": "Thôn Quý Dương, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9475076,
   "Latitude": 106.2326926
 },
 {
   "STT": 299,
   "Name": "Quầy thuốc Lan Anh",
   "address": "Thôn Phú An, xã Cao An, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.951047,
   "Latitude": 106.25113
 },
 {
   "STT": 300,
   "Name": "Quầy thuốc Đức Phúc của CTCPDTB  Y Tế Hà Dương",
   "address": "Khu 5, Thị trấnrấn Lai Cách, Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9366944,
   "Latitude": 106.2551563
 },
 {
   "STT": 301,
   "Name": "Quầy thuốc Ngọc Nam của CTCPDTB Y Tế Hà Dương",
   "address": "Khu 20, Thị trấnrấn Lai Cách, Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9364055,
   "Latitude": 106.2695611
 },
 {
   "STT": 302,
   "Name": "Quầy thuốc  Hoapharco 13",
   "address": "Thôn Thượng, xã Cẩm Đông,Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9688133,
   "Latitude": 106.1758232
 },
 {
   "STT": 303,
   "Name": "Quầy thuốc của hiệu thuốc Cẩm Giàng",
   "address": "Phố Ghẽ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 304,
   "Name": "Quầy thuốc của hiệu thuốc Cẩm Giàng",
   "address": "Ga Cao Xá, xã Cao An, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9477882,
   "Latitude": 106.2283367
 },
 {
   "STT": 305,
   "Name": "Quầy thuốc của hiệu thuốc Cẩm Giàng",
   "address": "Số 112 khu 18, Thị trấnrấn Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9366944,
   "Latitude": 106.2551563
 },
 {
   "STT": 306,
   "Name": "Quầy thuốc của hiệu thuốc Cẩm Giàng",
   "address": "Làng Gạch, Thị trấnrấn Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9366944,
   "Latitude": 106.2551563
 },
 {
   "STT": 307,
   "Name": "Quầy thuốc trung tâm HT Cẩm Giàng",
   "address": "Phố Ghẽ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 308,
   "Name": "Quầy thuốc bán thuốc cho HT H.Cẩm Giàng",
   "address": "Khu I Đường Độc Lập, Thị trấn.Cẩm Giàng, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.967546,
   "Latitude": 106.1728996
 },
 {
   "STT": 309,
   "Name": "Quầy thuốc Duẫn Quyên",
   "address": "Số 29 khu 3 Đường Độc Lập, Thị trấn.Cẩm Giàng, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9675494,
   "Latitude": 106.1719911
 },
 {
   "STT": 310,
   "Name": "Quầy thuốc bán thuốc cho HT H.Cẩm Giàng",
   "address": "Xóm Mới, thôn Tràng Kỹ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 311,
   "Name": "Quầy thuốc bán thuốc cho HT H.Cẩm Giàng",
   "address": "Xóm Mới, thôn Tràng Kỹ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 312,
   "Name": "Quầy thuốc bán thuốc cho HT H.Cẩm Giàng",
   "address": "Thôn Tràng Kỹ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9302578,
   "Latitude": 106.2154341
 },
 {
   "STT": 313,
   "Name": "Quầy thuốc bán thuốc cho HT H.Cẩm Giàng",
   "address": "Thôn Tràng Kỹ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9302578,
   "Latitude": 106.2154341
 },
 {
   "STT": 314,
   "Name": "Quầy thuốc bán thuốc cho HT H.Cẩm Giàng",
   "address": "Thôn Phú An, xã Cao An, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.951047,
   "Latitude": 106.25113
 },
 {
   "STT": 315,
   "Name": "Quầy thuốc Phong Hoà",
   "address": "Khu 20, thôn Tiền, Thị trấn.Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9364055,
   "Latitude": 106.2695611
 },
 {
   "STT": 316,
   "Name": "Quầy thuốc Thu Găng",
   "address": "Số 242 Trần Hưng Đạo, Thị trấn Kẻ Sặt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9096634,
   "Latitude": 106.1463296
 },
 {
   "STT": 317,
   "Name": "Quầy thuốc Phúc A",
   "address": "Thôn Phúc A, xã Cẩm Phúc, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9352589,
   "Latitude": 106.205622
 },
 {
   "STT": 318,
   "Name": "Quầy thuốc Hoapharco số 30",
   "address": "Thôn Phú Lộc, xã Cẩm Vũ, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9764743,
   "Latitude": 106.2487323
 },
 {
   "STT": 319,
   "Name": "Quầy thuốc Khánh Đình",
   "address": "Thôn Nghĩa, Thị trấn Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9366944,
   "Latitude": 106.2551563
 },
 {
   "STT": 320,
   "Name": "Quầy thuốc số 9 ",
   "address": "Thôn Phú Lộc, xã Cẩm Vũ, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9764743,
   "Latitude": 106.2487323
 },
 {
   "STT": 321,
   "Name": "Quầy thuốc tư nhân",
   "address": "Thôn Gạch, Thị trấn Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9354285,
   "Latitude": 106.248554
 },
 {
   "STT": 322,
   "Name": "Quầy thuốc số 15",
   "address": "Thôn Văn Thai, xã Cẩm Văn, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9872779,
   "Latitude": 106.259551
 },
 {
   "STT": 323,
   "Name": "Quầy thuốc số 48",
   "address": "196 khu 3,Thị trấn Cẩm Giàng, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9684918,
   "Latitude": 106.1735323
 },
 {
   "STT": 324,
   "Name": "Quầy thuốc Ngọc Nam",
   "address": "khu 20 Thị trấn Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9364055,
   "Latitude": 106.2695611
 },
 {
   "STT": 325,
   "Name": "Quầy thuốc Bảo Bình",
   "address": "thôn Đông Giao, xã Lương Điền, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9471109,
   "Latitude": 106.171714
 },
 {
   "STT": 326,
   "Name": "Quầy thuốc Trung Nga",
   "address": "Thôn Lôi Xá, xã Đức Chính, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9585936,
   "Latitude": 106.2741919
 },
 {
   "STT": 327,
   "Name": "Quầy thuốc số 06",
   "address": "Phố Ghẽ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 328,
   "Name": "Quầy thuốc số 10",
   "address": "Thôn Phí Xá, xã Cẩm Hoàng, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.974729,
   "Latitude": 106.230663
 },
 {
   "STT": 329,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Số 60, khu 18, Thị trấnr Lai Cách, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9366944,
   "Latitude": 106.2551563
 },
 {
   "STT": 330,
   "Name": "Đại lý Hoapharco số 2",
   "address": "Thôn Hỷ Duyệt, xã Cẩm Hưng, Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9479485,
   "Latitude": 106.22868
 },
 {
   "STT": 331,
   "Name": "Đại lý   Hoapharco 11",
   "address": "Thôn Quảng  Cư, xã Cẩm Đoài,Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9133425,
   "Latitude": 106.251303
 },
 {
   "STT": 332,
   "Name": "Đại lý bán thuốc cho  HT H.Cẩm Giàng",
   "address": "Thôn Phí Xá, xã Cẩm Hoàng, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.974729,
   "Latitude": 106.230663
 },
 {
   "STT": 333,
   "Name": "Đại lý -HT H.Cẩm Giàng",
   "address": "Phố Ghẽ, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.938127,
   "Latitude": 106.214577
 },
 {
   "STT": 334,
   "Name": "Đại lý bán thuốc cho  HT H.Cẩm Giàng",
   "address": "Thôn Quý Dương, xã Tân Trường, Huyện Cẩm Giàng,Tỉnh Hải Dương",
   "Longtitude": 20.9475076,
   "Latitude": 106.2326926
 },
 {
   "STT": 335,
   "Name": "Nhà thuốc Hòa Sinh ngoài giờ",
   "address": "Phố Phúc Lâm, Thị trấnr Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9886519,
   "Latitude": 106.553525
 },
 {
   "STT": 336,
   "Name": "Chi nhánh công ty CPDược &VTYT tại Kinh Môn",
   "address": "Phố Phúc Lâm, Thị trấnr Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9886519,
   "Latitude": 106.553525
 },
 {
   "STT": 337,
   "Name": "Doanh nghiệp tư nhân dược phẩm Cao Minh",
   "address": "Thôn Vũ Xá, xã Thất Hùng, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0517998,
   "Latitude": 106.4823979
 },
 {
   "STT": 338,
   "Name": "Quầy thuốc",
   "address": "Phố Cộng Hòa, Thị trấnr Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9897301,
   "Latitude": 106.5502167
 },
 {
   "STT": 339,
   "Name": "Quầy thuốc hiệu thuốc Kinh Môn",
   "address": "Thôn Vạn Chánh, Thị trấnr Phú Thứ, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.020046,
   "Latitude": 106.556909
 },
 {
   "STT": 340,
   "Name": "Quầy thuốc trung tâm GPP hiệu thuốc Kinh Môn",
   "address": "Thị trấn Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9944813,
   "Latitude": 106.5495683
 },
 {
   "STT": 341,
   "Name": "Quầy thuốc GPP bệnh viện",
   "address": "Thị trấn Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9944813,
   "Latitude": 106.5495683
 },
 {
   "STT": 342,
   "Name": "Quầy thuốc của hiệu thuốc Kinh Môn",
   "address": "Khu 2, Thị trấn Minh Tân, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0405072,
   "Latitude": 106.5829611
 },
 {
   "STT": 343,
   "Name": "Quầy thuốc hiệu thuốc Kinh Môn",
   "address": "Khu II, Hạ Chiểu, Thị trấn Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0110318,
   "Latitude": 106.5273651
 },
 {
   "STT": 344,
   "Name": "Quầy thuốc  hiệu thuốc Kinh Môn",
   "address": "Khu II, Thị trấn Minh Tân, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0405072,
   "Latitude": 106.5829611
 },
 {
   "STT": 345,
   "Name": "Quầy thuốc hoapharco số 25",
   "address": "Thị trấn Phú Thứ, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.02133,
   "Latitude": 106.5565688
 },
 {
   "STT": 346,
   "Name": "Quầy thuốc BVĐK Nhị Chiểu",
   "address": "Khu II, Thị trấn Phú Thứ, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0212799,
   "Latitude": 106.5566117
 },
 {
   "STT": 347,
   "Name": "Quầy thuốc Hồng Thắm",
   "address": "Chợ Thống Nhất, xã Hiến Thành, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9645607,
   "Latitude": 106.5611265
 },
 {
   "STT": 348,
   "Name": "Quầy thuốc Thu Quyên",
   "address": "Thôn Miêu Nha, xã Phúc Thành, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0110318,
   "Latitude": 106.5273651
 },
 {
   "STT": 349,
   "Name": "Quầy thuốc số 9",
   "address": "thôn Hiệp Thượng, xã Hiệp sơn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0077789,
   "Latitude": 106.5322391
 },
 {
   "STT": 350,
   "Name": "Quầy thuốc số 01",
   "address": "phố Phúc Lâm, Thị trấn Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9886519,
   "Latitude": 106.553525
 },
 {
   "STT": 351,
   "Name": "Quầy thuốc số 03",
   "address": "thôn Thái Mông, xã Phúc Thành,Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0336178,
   "Latitude": 106.433208
 },
 {
   "STT": 352,
   "Name": "Quầy thuốc số 02",
   "address": "phố Cộng Hòa, Thị trấn Kinh Môn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9897301,
   "Latitude": 106.5502167
 },
 {
   "STT": 353,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Thôn Kim Lôi, xã Bạch Đằng, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9659229,
   "Latitude": 106.5209377
 },
 {
   "STT": 354,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Thôn Thái Mông, xã Phúc Thành, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.033323,
   "Latitude": 106.4324435
 },
 {
   "STT": 355,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Thôn Duẩn Khê, xã Long Xuyên, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9725801,
   "Latitude": 106.5476847
 },
 {
   "STT": 356,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Thôn Hà Tràng, xã Thăng Long, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0008685,
   "Latitude": 106.4362335
 },
 {
   "STT": 357,
   "Name": "Đại lý bán thuốc Công ty TNHH DP Việt Dũng",
   "address": "Thôn Huyền Tụng, xã Hiến Thành, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9534384,
   "Latitude": 106.5554608
 },
 {
   "STT": 358,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Thôn Phượng Hoàng, xã Thất Hùng, Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0127352,
   "Latitude": 106.5112712
 },
 {
   "STT": 359,
   "Name": "Đại lý Hoapharco  số 26",
   "address": "Thôn Nghĩa Vũ, xã An Sinh, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0152109,
   "Latitude": 106.5265703
 },
 {
   "STT": 360,
   "Name": "Đại lý thuốchiệu thuốc Kinh Môn",
   "address": "Thôn Vũ Xá, Thất Hùng, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0517998,
   "Latitude": 106.4823979
 },
 {
   "STT": 361,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Thôn Hiệp Thượng, xã Hiệp Sơn, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0077789,
   "Latitude": 106.5322391
 },
 {
   "STT": 362,
   "Name": "Đại lý hiệu thuốc Kinh Môn",
   "address": "Khu II, Bích Nhôi, Thị trấn Minh Tân, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 21.0405072,
   "Latitude": 106.5829611
 },
 {
   "STT": 363,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Thôn Thượng Xá, xã Thượng Quận, Huyện Kinh Môn,Tỉnh Hải Dương",
   "Longtitude": 20.9967242,
   "Latitude": 106.4962895
 },
 {
   "STT": 364,
   "Name": "Chi nhánh công ty CPDược &VTYT tại Nam Sách",
   "address": "Số 385 Trần Phú, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9965514,
   "Latitude": 106.3347816
 },
 {
   "STT": 365,
   "Name": "Quầy thuốc Thanh Hạnh",
   "address": "Phố Na, xã Nam Chính, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.995464,
   "Latitude": 106.3346061
 },
 {
   "STT": 366,
   "Name": "Quầy thuốc Tuấn Khánh",
   "address": "227 Nguyễn Trãi, Thị trấnr Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9915281,
   "Latitude": 106.3392931
 },
 {
   "STT": 367,
   "Name": "Quầy thuốc bệnh viện đa khoa Nam Sách",
   "address": "Số 354 Trần Phú, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9940624,
   "Latitude": 106.3345296
 },
 {
   "STT": 368,
   "Name": "Quầy thuốc Tuyền Hiếu",
   "address": "Thôn Thương Dương, xã Nam Trung, Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0128473,
   "Latitude": 106.3300708
 },
 {
   "STT": 369,
   "Name": "Quầy thuốc  Khánh Vân",
   "address": "Thôn Miễu Lãng, xã Đồng Lạc, Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9823812,
   "Latitude": 106.3574169
 },
 {
   "STT": 370,
   "Name": "Quầy thuốc Bảo Ngọc",
   "address": "Thôn Đột Hạ, xã Nam Tân, Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0700123,
   "Latitude": 106.3485866
 },
 {
   "STT": 371,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "Thôn Van Tải, xã Hồng phong, Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9833832,
   "Latitude": 106.3176836
 },
 {
   "STT": 372,
   "Name": "Quầy thuốc Hưng Phong",
   "address": "Số 444 Trần Phú, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9951591,
   "Latitude": 106.3346421
 },
 {
   "STT": 373,
   "Name": "Quầy thuốc  Đức Anh",
   "address": "Chợ La, thôn La Đôi, xã Hợp Tiến, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0481035,
   "Latitude": 106.3287199
 },
 {
   "STT": 374,
   "Name": "Quầy thuốc Bách Hoa",
   "address": "Thôn An Đông, xã An Bình, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.995464,
   "Latitude": 106.3346061
 },
 {
   "STT": 375,
   "Name": "Quầy thuốc số 1 -  ",
   "address": "Số 404 Trần Phú, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9941775,
   "Latitude": 106.334567
 },
 {
   "STT": 376,
   "Name": "Quầy thuốc Trâm Anh",
   "address": "Thôn Thượng Dương, xã Nam Trung, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0128473,
   "Latitude": 106.3300708
 },
 {
   "STT": 377,
   "Name": "Quầy thuốc Quân Thanh",
   "address": "Thôn Đầu, xã Hợp Tiến, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0555511,
   "Latitude": 106.3485866
 },
 {
   "STT": 378,
   "Name": "Quầy thuốc Thuận Hải",
   "address": "Thôn Linh Khê, xã Thanh Quang, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0468516,
   "Latitude": 106.358222
 },
 {
   "STT": 379,
   "Name": "Quầy thuốc Hoapharco số 20",
   "address": "Thôn Miễu Lãng, xã Đồng Lạc, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9823812,
   "Latitude": 106.3574169
 },
 {
   "STT": 380,
   "Name": "Quầy thuốc Hoàng Hà",
   "address": "Thôn Tiền, Thị trấn Lai Cách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9366944,
   "Latitude": 106.2551563
 },
 {
   "STT": 381,
   "Name": "Quầy thuốc trung tâm-HT Nam Sách",
   "address": "Số 385 Trần Phú, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9965514,
   "Latitude": 106.3347816
 },
 {
   "STT": 382,
   "Name": "Quầy thuốc Hoapharco số 21",
   "address": "Thôn Kim Độ, xã Hiệp Cát, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0394998,
   "Latitude": 106.3154979
 },
 {
   "STT": 383,
   "Name": "Quầy thuốc Lan Nhi",
   "address": "Thôn Lê Hà, xã Thanh Quang, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.040835,
   "Latitude": 106.3524464
 },
 {
   "STT": 384,
   "Name": "Quầy thuốc Thanh Long Na",
   "address": "Thôn Đào Lâm, xã Đoàn Tùng, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.8243987,
   "Latitude": 106.2198553
 },
 {
   "STT": 385,
   "Name": "Quầy thuốc Hoapharco  số 23",
   "address": "Khu Vô Hối, Thị trấnr Thanh Miện, Huyện Thanh Miện,Tỉnh Hải Dương",
   "Longtitude": 20.7879784,
   "Latitude": 106.237505
 },
 {
   "STT": 386,
   "Name": "Quầy thuốc Hải Hà",
   "address": "Thôn An Xá, xã Quốc Tuấn, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0280027,
   "Latitude": 106.3419641
 },
 {
   "STT": 387,
   "Name": "Quầy thuốc Lợi Chi",
   "address": "Thôn Lâm Xuyên, xã Phú Điền, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9937769,
   "Latitude": 106.370101
 },
 {
   "STT": 388,
   "Name": "Quầy thuốc Toản Thơm",
   "address": "Thôn Uông Hạ, xã Minh Tân, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9714165,
   "Latitude": 106.2995699
 },
 {
   "STT": 389,
   "Name": "Quầy thuốc Hải Thủy",
   "address": "Thôn An Xá, xã Quốc Tuấn, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0280027,
   "Latitude": 106.3419641
 },
 {
   "STT": 390,
   "Name": "Quầy thuốc số 02",
   "address": "Số 35 Trần Hưng Đạo, khu Mạc Thị Bưởi, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9922832,
   "Latitude": 106.3375493
 },
 {
   "STT": 391,
   "Name": "Quầy thuốc số 10",
   "address": "Số 249 Trần Phú, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9923541,
   "Latitude": 106.3342849
 },
 {
   "STT": 392,
   "Name": "Quầy Hoapharco 28",
   "address": "Thôn Hảo Thôn, xã Đồng Lạc, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9755454,
   "Latitude": 106.3691998
 },
 {
   "STT": 393,
   "Name": "Quầy thuốc số 11",
   "address": "Số 602 Trần Phú, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9985147,
   "Latitude": 106.3349265
 },
 {
   "STT": 394,
   "Name": "Quầy thuốc số 17",
   "address": "Chợ Rồng, xã Thanh Quang, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0392155,
   "Latitude": 106.3526317
 },
 {
   "STT": 395,
   "Name": "Quầy thuốc số 03",
   "address": "Thôn An Điền, xã Cộng Hòa, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.005732,
   "Latitude": 106.3988969
 },
 {
   "STT": 396,
   "Name": "Quầy thuốc số 23",
   "address": "Thôn Lý Văn, xã Phú Điền, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9946001,
   "Latitude": 106.37287069999999
 },
 {
   "STT": 397,
   "Name": "Quầy thuốc Thúy Hoàng",
   "address": "Thôn Lãng Khê, xã An Lâm, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0041638,
   "Latitude": 106.3366431
 },
 {
   "STT": 398,
   "Name": "Quầy thuốc số 09",
   "address": "Số 552 Trần Phú, Thị trấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9974772,
   "Latitude": 106.3349119
 },
 {
   "STT": 399,
   "Name": "Quầy thuốc Thu Thủy",
   "address": "343 Nguyễn Trãi, khu Đồng Khê, Thị trấn Nam sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9426784,
   "Latitude": 106.3272483
 },
 {
   "STT": 400,
   "Name": "Quầy thuốc Thùy Vân ",
   "address": "thôn Đa Đinh, xã An Bình, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.018675,
   "Latitude": 106.370663
 },
 {
   "STT": 401,
   "Name": "Quầy thuốc Thủy Nụ",
   "address": "Cõi, xã An Sơn, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0109309,
   "Latitude": 106.3095908
 },
 {
   "STT": 402,
   "Name": "Đại lý số 6 ",
   "address": "Thôn Đông, xã Quốc Tuấn, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0355782,
   "Latitude": 106.3415654
 },
 {
   "STT": 403,
   "Name": "Đại lý số 8 ",
   "address": "328 Trần Phú, Thị trấnrấn Nam Sách, Huyện Nam Sách ,Tỉnh Hải Dương",
   "Longtitude": 20.9923933,
   "Latitude": 106.3343622
 },
 {
   "STT": 404,
   "Name": "Đại lý số 13 ",
   "address": "Thôn Bịch Tây, xã Nam Chính, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0327085,
   "Latitude": 106.3211346
 },
 {
   "STT": 405,
   "Name": "Đại lý số 14",
   "address": "Thôn Cổ Pháp, xã Cộng Hoà, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0033925,
   "Latitude": 106.3858938
 },
 {
   "STT": 406,
   "Name": "Đại lý số 28 ",
   "address": "Thôn Miễu Lãng, xã Đồng Lạc, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9823812,
   "Latitude": 106.3574169
 },
 {
   "STT": 407,
   "Name": "Đại lý số 25 ",
   "address": "Thôn Mạn Đê, xã  Nam Trung, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0129004,
   "Latitude": 106.327296
 },
 {
   "STT": 408,
   "Name": "Đại  lý số 16 ",
   "address": "Số 434 Trần Phú, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9948964,
   "Latitude": 106.3346069
 },
 {
   "STT": 409,
   "Name": "Đại lý số  9 ",
   "address": "Số 552 Trần Phú, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9974772,
   "Latitude": 106.3349119
 },
 {
   "STT": 410,
   "Name": "Đại lý số 14 ",
   "address": "Thôn Đột Hạ, xã Nam Tân, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0700123,
   "Latitude": 106.3485866
 },
 {
   "STT": 411,
   "Name": "Đại lý số 32 ",
   "address": "Số 123 Nguyễn Trãi, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9912026,
   "Latitude": 106.3361174
 },
 {
   "STT": 412,
   "Name": "Đại  lý hoapharco số 13",
   "address": "Thôn Miễu Lãng, xã Đồng Lạc, Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9823812,
   "Latitude": 106.3574169
 },
 {
   "STT": 413,
   "Name": "Đại lý số 7",
   "address": "Đường 37, Thôn Miễu Lãng, xã Đồng Lạc, Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9823812,
   "Latitude": 106.3574169
 },
 {
   "STT": 414,
   "Name": "Đại lý số 24 ",
   "address": "Số 253 Trần Phú, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9924697,
   "Latitude": 106.334331
 },
 {
   "STT": 415,
   "Name": "Đại lý số 2 ",
   "address": "Số 35 Trần Hưng Đạo, khu Mạc Thị Bưởi, Thị trấnrấn Nam Sách, Huyện Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9890489,
   "Latitude": 106.3357901
 },
 {
   "STT": 416,
   "Name": "Đại lý Hoapharco số 25",
   "address": "Đường Trần Hưng Đạo, khu La Xuyên, Thị trấn Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 20.9880072,
   "Latitude": 106.3433862
 },
 {
   "STT": 417,
   "Name": "Đại lý số 20",
   "address": "Số 93 Nguyễn Đăng Lành, khu Nguyễn Văn Trỗi, Thị trấn Nam Sách,Tỉnh Hải Dương",
   "Longtitude": 21.0027802,
   "Latitude": 106.3343723
 },
 {
   "STT": 418,
   "Name": "Chi nhánh CTCP Dược VTYT  tại Ninh Giang",
   "address": "Số 101 Trần Hưng Đạo, khu 3, Thị trấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7308283,
   "Latitude": 106.3991954
 },
 {
   "STT": 419,
   "Name": "Quầy thuốc GPP BVĐK Ninh Giang",
   "address": "Khu 6, Thị trấnrấn Ninh Giang, Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7317976,
   "Latitude": 106.396244
 },
 {
   "STT": 420,
   "Name": "Quầy thuốc Thu Phương",
   "address": "Thôn 6, xã Tân Hương, Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 421,
   "Name": "Quầy thuốc Hưng Thái",
   "address": "Thôn An Lý, xã Hưng Thái, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7334723,
   "Latitude": 106.2775901
 },
 {
   "STT": 422,
   "Name": "Quầy thuốc của hiệu thuốc Ninh Giang",
   "address": "Thôn Đồng Lạc, xã Hồng Đức, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7789468,
   "Latitude": 106.3108956
 },
 {
   "STT": 423,
   "Name": "Quầy thuốc Đức Phúc",
   "address": "Số 271, khu 6 Cống Sao, Thị trấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7275578,
   "Latitude": 106.3945659
 },
 {
   "STT": 424,
   "Name": "Quầy thuốc trung tâm HT Ninh Giang",
   "address": "Số 101 Trần Hưng Đạo, khu 3, Thị trấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7308283,
   "Latitude": 106.3991954
 },
 {
   "STT": 425,
   "Name": "Quầy thuốc Hùng Đào ",
   "address": "Thôn 6, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 426,
   "Name": "Quốc số 16 – hiệu thuốc Ninh Giang",
   "address": "Thôn Lang Viên, xã Hồng Dụ, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7326761,
   "Latitude": 106.4004421
 },
 {
   "STT": 427,
   "Name": "Quầy số 26- HT Ninh Giang",
   "address": "Thôn Hữu Chung, xã Tân Phong, Huyện \\Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7333183,
   "Latitude": 106.4042187
 },
 {
   "STT": 428,
   "Name": "Quầy thuốc Loan Tuyên",
   "address": "Thôn 5, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 429,
   "Name": "Quầy thuốc Cầu Ràm",
   "address": "Khu Cầu Ràm, xã Nghĩa An, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7776217,
   "Latitude": 106.3665521
 },
 {
   "STT": 430,
   "Name": "Quầy thuốc Hoài Nga",
   "address": "Thôn Bồ Dương, xã Hồng Phong, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.718555,
   "Latitude": 106.341138
 },
 {
   "STT": 431,
   "Name": "Quầy thuốc số 1",
   "address": "Cầu Ràm, xã Nghĩa An, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7835169,
   "Latitude": 106.366931
 },
 {
   "STT": 432,
   "Name": "Quầy thuốc số 02",
   "address": "Thôn Dâm Me, xã Đồng Tâm, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7267,
   "Latitude": 106.398201
 },
 {
   "STT": 433,
   "Name": "Quầy thuốc số 03",
   "address": "Thôn Ngọc Hòa, xã Vĩnh Hòa, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.753987,
   "Latitude": 106.3841343
 },
 {
   "STT": 434,
   "Name": "Quầy thuốc số 28",
   "address": "Thôn Ngọc Hòa, xã Vĩnh Hòa, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.753987,
   "Latitude": 106.3841343
 },
 {
   "STT": 435,
   "Name": "Quầy thuốc số 05",
   "address": "Chợ Đọ, xã Ứng Hòe, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7267,
   "Latitude": 106.398201
 },
 {
   "STT": 436,
   "Name": "Quầy thuốc Ngọc Yến",
   "address": "Thôn Đông Cao, xã Đông Xuyên, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7585756,
   "Latitude": 106.342798
 },
 {
   "STT": 437,
   "Name": "Quầy thuốc số 12",
   "address": "Số 108 Đường Trần Hưng Đạo, Thị trấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7314093,
   "Latitude": 106.4017457
 },
 {
   "STT": 438,
   "Name": "Quầy thuốc số 13",
   "address": "Số 162 Đường Trần Hưng Đạo, Thị trấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7313896,
   "Latitude": 106.4003734
 },
 {
   "STT": 439,
   "Name": "Quầy thuốc số 07",
   "address": "Thôn Đà Phố, xã Hồng Phúc, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7156922,
   "Latitude": 106.3142688
 },
 {
   "STT": 440,
   "Name": "Quầy thuốc Đức Phúc",
   "address": "Thôn Cúc Thị, xã Kiến Quốc, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7200126,
   "Latitude": 106.3273573
 },
 {
   "STT": 441,
   "Name": "Quầy thuốc số 58",
   "address": "Chợ Bùi, xã Hoàng Hanh, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7612726,
   "Latitude": 106.2833616
 },
 {
   "STT": 442,
   "Name": "Quầy thuốc số 06",
   "address": "Thôn Ứng Mộ, xã An Đức, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7647996,
   "Latitude": 106.2993792
 },
 {
   "STT": 443,
   "Name": "Quầy thuốc số 08",
   "address": "Thôn Bùi Hòa, xã Hoàng Hanh, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7585668,
   "Latitude": 106.2806869
 },
 {
   "STT": 444,
   "Name": "Quầy thuốc số 22",
   "address": "Thôn Đoàn Xá, xã Tân Quang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.744705,
   "Latitude": 106.277518
 },
 {
   "STT": 445,
   "Name": "Đaị lý bán thuốc số 38 hiệu thuốc Ninh Giang",
   "address": "Thôn Mai Động, xã hồng Đức, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7789468,
   "Latitude": 106.3108956
 },
 {
   "STT": 446,
   "Name": "Đại lý số 27 hiệu thuốc Ninh Giang",
   "address": "Thôn Cúc Bồ, xã Kiến Quốc, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7211967,
   "Latitude": 106.3266277
 },
 {
   "STT": 447,
   "Name": "Đại lý bán thuốc hiệu thuốc Ninh Giang",
   "address": "Thôn Văn Diêm, xã hưng Thái, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7227092,
   "Latitude": 106.2944674
 },
 {
   "STT": 448,
   "Name": "Đại lý bán thuốc  cho hiệu thuốc Ninh Giang",
   "address": "Thị tứ Tuy Hoà, xã Văn Hội, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7331785,
   "Latitude": 106.2770917
 },
 {
   "STT": 449,
   "Name": "Đại lý Hà Phương số 38 hiệu thuốc Ninh Giang",
   "address": "Thôn Đa Nghi, xã Nghĩa An, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7864501,
   "Latitude": 106.3624926
 },
 {
   "STT": 450,
   "Name": "Đại lý bán thuốchiệu thuốc Ninh Giang",
   "address": "Thôn Cúc Bồ, xã Kiến Quốc, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7211967,
   "Latitude": 106.3266277
 },
 {
   "STT": 451,
   "Name": "Đại lý bán thuốc hiệu thuốc Ninh Giang",
   "address": "Thôn Đồng Lạc, xã Hồng Đức,Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7789468,
   "Latitude": 106.3108956
 },
 {
   "STT": 452,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Số 28 khu 2, Thị trấnrấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7318419,
   "Latitude": 106.4002983
 },
 {
   "STT": 453,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Số 313 khu 3, Thị trấnrấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7308283,
   "Latitude": 106.3991954
 },
 {
   "STT": 454,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thôn Mai Động, xã Hồng Đức, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7789468,
   "Latitude": 106.3108956
 },
 {
   "STT": 455,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thôn 4, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 456,
   "Name": "Đại lý bán thuốc hiệu thuốc huyện Ninh Giang",
   "address": "Thôn Đỗ Xá, xã ứng Hoè,  Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7267,
   "Latitude": 106.398201
 },
 {
   "STT": 457,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thôn Vé, xã Đồng Tâm,. Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7491091,
   "Latitude": 106.3699271
 },
 {
   "STT": 458,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thôn Tranh Xuyên, xã Đồng Tâm, Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7389702,
   "Latitude": 106.3878984
 },
 {
   "STT": 459,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thôn Ngọc Hoà, xã Vĩnh Hoà, Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.753987,
   "Latitude": 106.3841343
 },
 {
   "STT": 460,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thôn Thọ Sơn, xã Quang Hưng, Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7487784,
   "Latitude": 106.2894922
 },
 {
   "STT": 461,
   "Name": "Đại lý hiệu thuốc Ninh Giang",
   "address": "Thị tứ Tuy Hoà, xã Tân Quang, Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7398127,
   "Latitude": 106.2776145
 },
 {
   "STT": 462,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Ninh Giang",
   "address": "Thôn Vĩnh Xuyên, xã Vĩnh Hòa, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.754308,
   "Latitude": 106.3864088
 },
 {
   "STT": 463,
   "Name": "Đại lý bán thuốc cho  hiệu thuốc Ninh",
   "address": "Thôn Văn Diệm, xã Hưng Thái, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7227092,
   "Latitude": 106.2944674
 },
 {
   "STT": 464,
   "Name": "Đại lý bán thuốc cho  hiệu thuốc Ninh Giang",
   "address": "Thôn 3, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 465,
   "Name": "Đại lý bán thuốc cho  hiệu thuốc Ninh Giang",
   "address": "Thôn Trịnh Xuyên, xã Nghĩa An, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7846206,
   "Latitude": 106.3531524
 },
 {
   "STT": 466,
   "Name": "Đại lý bán thuốc cho  hiệu thuốc Ninh Giang",
   "address": "Thôn 7, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 467,
   "Name": "Đại lý bán thuốc cho  hiệu thuốc Ninh Giang",
   "address": "Thôn Văn Hội, xã Văn Hội, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7334723,
   "Latitude": 106.2775901
 },
 {
   "STT": 468,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Ninh Giang",
   "address": "Thôn Hữu Chung ,  xã Tân  Phong ,  Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7491091,
   "Latitude": 106.3699271
 },
 {
   "STT": 469,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Ninh Giang",
   "address": "Thôn Nam, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 470,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Ninh Giang",
   "address": "Thôn An Cư, xã Nghĩa An, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7835169,
   "Latitude": 106.366931
 },
 {
   "STT": 471,
   "Name": "Đại lý bán thuốc cho hiệu thuốc Ninh Giang",
   "address": "Thôn Hào Khê, xã Hưng Long, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7068428,
   "Latitude": 106.3018906
 },
 {
   "STT": 472,
   "Name": "Đại lý số 31-Hiệu thuốc Ninh Giang",
   "address": "Chợ Đọ, xã Ứng Hoè, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7267,
   "Latitude": 106.398201
 },
 {
   "STT": 473,
   "Name": "Đại lý bán thuốc cho HT Ninh Giang",
   "address": "Thôn I, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 474,
   "Name": "Đại lý số 30-HT Ninh Giang",
   "address": "Thôn Đoàn Xá,xã Tân Quang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.744705,
   "Latitude": 106.277518
 },
 {
   "STT": 475,
   "Name": "Đại lý số 36-HT Ninh Giang",
   "address": "Thôn Ứng Mộ, xã An Đức, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7647996,
   "Latitude": 106.2993792
 },
 {
   "STT": 476,
   "Name": "Đại lý bán thuốc cho HT Ninh Giang",
   "address": "53 khu II, Thị trấnr. Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7308459,
   "Latitude": 106.4004045
 },
 {
   "STT": 477,
   "Name": "Đại lý số 32-HT Ninh Giang",
   "address": "Thôn 2, xã Văn Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7030743,
   "Latitude": 106.2777109
 },
 {
   "STT": 478,
   "Name": "Đại lý số 47 – Hiệu thuốc Ninh Giang",
   "address": "Số 39 Khu 2, Thị trấn Ninh Giang, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7316312,
   "Latitude": 106.4003663
 },
 {
   "STT": 479,
   "Name": "Đại lý số 68-HT Ninh Giang",
   "address": "Thôn Tiêu, xã Hồng Thái, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7303015,
   "Latitude": 106.3552092
 },
 {
   "STT": 480,
   "Name": "Đại lý số 36-HT Ninh Giang",
   "address": "Thôn 7, xã Tân Hương, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.769469,
   "Latitude": 106.3642
 },
 {
   "STT": 481,
   "Name": "Đại lý số 57- HT Ninh Giang",
   "address": "Thôn Cúc Thị, xã Kiến Quốc, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.7200126,
   "Latitude": 106.3273573
 },
 {
   "STT": 482,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Thôn Do Nghĩa, xã Nghĩa An, Huyện Ninh Giang,Tỉnh Hải Dương",
   "Longtitude": 20.780762,
   "Latitude": 106.364745
 },
 {
   "STT": 483,
   "Name": "Chi nhánh CTCP dược VTYT  tại Thanh Hà",
   "address": "Số 150, Thị trấnrấn Thanh Hà, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9058446,
   "Latitude": 106.428155
 },
 {
   "STT": 484,
   "Name": "Quầy thuốc GPP trung tâm hiệu thuốc Thanh  Hà ",
   "address": "Số 150 Thị trấnrấn Thanh Hà, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9058446,
   "Latitude": 106.428155
 },
 {
   "STT": 485,
   "Name": "Quầy thuốc hiệu thuốc Thanh Hà",
   "address": "Khu 5, Thị trấnrấn Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9039237,
   "Latitude": 106.4256781
 },
 {
   "STT": 486,
   "Name": "Quầy thuốc hiệu thuốc Thanh Hà",
   "address": "Xã Thanh Lang, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9416392,
   "Latitude": 106.4630942
 },
 {
   "STT": 487,
   "Name": "Quầy thuốc hiệu thuốc Thanh Hà",
   "address": "Chợ Hệ, xã Thanh Bính, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8517667,
   "Latitude": 106.4729811
 },
 {
   "STT": 488,
   "Name": "Quầy thuốc số 9 hiệu thuốc Thanh Hà",
   "address": "Xóm 9, xã Phượng Hoàng, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8575493,
   "Latitude": 106.4140868
 },
 {
   "STT": 489,
   "Name": "Quầy thuốc số 3 hiệu thuốc Thanh Hà",
   "address": "Khu 2, Thị trấnrấn Thanh Hà, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.902638,
   "Latitude": 106.425848
 },
 {
   "STT": 490,
   "Name": "Quầy thuốc trung tâm hiệu  thuốc Thanh  Hà",
   "address": "Số 150, Thị trấnrấn Thanh Hà, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9058446,
   "Latitude": 106.428155
 },
 {
   "STT": 491,
   "Name": "Quầy thuốc hiệu thuốc Thanh Hà",
   "address": "Thôn Du La, xã Cẩm Chế, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9314027,
   "Latitude": 106.4319496
 },
 {
   "STT": 492,
   "Name": "Quầy thuốc của hiệu thuốc Thanh Hà",
   "address": "Thôn Hoàng Xá, xã Quyết Thắng, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9316713,
   "Latitude": 106.3830752
 },
 {
   "STT": 493,
   "Name": "Quầy thuốc của hiệu thuốc Thanh Hà",
   "address": "Thôn Lại Xá, xã Thanh Thủy, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8818611,
   "Latitude": 106.4503956
 },
 {
   "STT": 494,
   "Name": "Quầy thuốc HT Thanh Hà",
   "address": "Thôn Phù Tinh, xã Trường Thành, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8431108,
   "Latitude": 106.4563501
 },
 {
   "STT": 495,
   "Name": "Quầy thuốc HT Thanh Hà",
   "address": "Thôn Phúc Giới, xã Thanh Bính, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8461764,
   "Latitude": 106.4709091
 },
 {
   "STT": 496,
   "Name": "Quầy thuốc Đào Yến",
   "address": "An Liệt, Thanh Hải, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8886276,
   "Latitude": 106.4067262
 },
 {
   "STT": 497,
   "Name": "Quầy thuốc Nhâm Bằng ",
   "address": "Xóm Bắc, xã Hồng Lạc, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9295379,
   "Latitude": 106.4170311
 },
 {
   "STT": 498,
   "Name": "Quầy thuốc tân dược",
   "address": "Xóm 2, Thanh Xá, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8916197,
   "Latitude": 106.4421397
 },
 {
   "STT": 499,
   "Name": "Quầy thuốc Thanh Thúy",
   "address": "Du La, xã Cẩm Chế, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9311162,
   "Latitude": 106.4318262
 },
 {
   "STT": 500,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Thôn Cập Thượng, xã Tiền Tiến, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9058217,
   "Latitude": 106.3640398
 },
 {
   "STT": 501,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Thôn Cập Nhất, xã Tiền Tiến, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.909663,
   "Latitude": 106.3755401
 },
 {
   "STT": 502,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Khu 2, Thị trấnrấn Thanh Hà, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.902638,
   "Latitude": 106.425848
 },
 {
   "STT": 503,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm Đoài, xã Hồng Lạc, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9554894,
   "Latitude": 106.3956858
 },
 {
   "STT": 504,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm chợ,  xã Phượng Hoàng, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9295379,
   "Latitude": 106.4170311
 },
 {
   "STT": 505,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Thôn Xuân An, xã Thanh Khê, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8861793,
   "Latitude": 106.425611
 },
 {
   "STT": 506,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm 15, xã Liên Mạc, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9295379,
   "Latitude": 106.4170311
 },
 {
   "STT": 507,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm Đông, xã Hồng Lạc, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.958113,
   "Latitude": 106.4060788
 },
 {
   "STT": 508,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm Nam, xã Hồng Lạc, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.958113,
   "Latitude": 106.4060788
 },
 {
   "STT": 509,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Thôn Nhân Lư, xã Cẩm Chế, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9248721,
   "Latitude": 106.4307536
 },
 {
   "STT": 510,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm chợ, xã Phượng Hoàng, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8615334,
   "Latitude": 106.4173873
 },
 {
   "STT": 511,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm mới, xã Hồng Lạc, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.958113,
   "Latitude": 106.4060788
 },
 {
   "STT": 512,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Xóm 11, xã Thanh Sơn, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8699837,
   "Latitude": 106.4478773
 },
 {
   "STT": 513,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Thôn Xuân An, xã Thanh Khê, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8861793,
   "Latitude": 106.425611
 },
 {
   "STT": 514,
   "Name": "Đại lý hiệu thuốc Thanh Hà",
   "address": "Thôn An Liệt, xã Thanh Hải, Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8874126,
   "Latitude": 106.4017962
 },
 {
   "STT": 515,
   "Name": "Đại lý bán thuốc hiệu thuốc Thanh Hà",
   "address": "Khu 5, Thị trấnrấn Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9039237,
   "Latitude": 106.4256781
 },
 {
   "STT": 516,
   "Name": "Đại lý bán thuốc cho CN Công ty CP Dược VTYT tại Thanh Hà",
   "address": "Thôn Tiên Kiều, xã Thanh Hồng, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8313817,
   "Latitude": 106.4694462
 },
 {
   "STT": 517,
   "Name": "Đại lý bán thuốc cho CNCTCP dược VTYT tại H.Thanh Hà",
   "address": "Thôn Chợ Hệ, xã Thanh Bính, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.8517667,
   "Latitude": 106.4729811
 },
 {
   "STT": 518,
   "Name": "Đại lý bán thuốc cho doanh nghiệp",
   "address": "Thôn Nhân Lư, xã Cẩm Chế, Huyện Thanh Hà,Tỉnh Hải Dương",
   "Longtitude": 20.9248721,
   "Latitude": 106.4307536
 },
 {
   "STT": 519,
   "Name": "Chi nhánh CTCP dược VTYT  tại Binhg Giang",
   "address": "Thị trấnr Kẻ Sặt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9109309,
   "Latitude": 106.1478019
 },
 {
   "STT": 520,
   "Name": "Quầy thuốc Biên Huyền",
   "address": "Thôn Cậy, xã Long Xuyên, Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8926926,
   "Latitude": 106.2316216
 },
 {
   "STT": 521,
   "Name": "Quầy thuốc BVĐK Bình Giang",
   "address": "Xã Bình Minh, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8727975,
   "Latitude": 106.1830901
 },
 {
   "STT": 522,
   "Name": "Quầy thuốc trung tâm hiệu thuốc huyện Bình Giang",
   "address": "Thị trấnr Kẻ Sặt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9109309,
   "Latitude": 106.1478019
 },
 {
   "STT": 523,
   "Name": "Quầy thuốc số 3 -CTTNHH DP Thành Đông",
   "address": "Xóm Mới, xã Bình Xuyên, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8372205,
   "Latitude": 106.1916808
 },
 {
   "STT": 524,
   "Name": "Quầy thuốc Thông Hiến",
   "address": "Thôn Sồi Cầu, xã Thái Học, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.863517,
   "Latitude": 106.2001763
 },
 {
   "STT": 525,
   "Name": "Quầy thuốc Lam Sơn",
   "address": "Khu trung tâm xã Cổ Bì, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8583203,
   "Latitude": 106.2444266
 },
 {
   "STT": 526,
   "Name": "Quầy thuốc số 07-CN CTCP Dược VTYT HD tại H.Bình Giang",
   "address": "Số 20 Thanh Niên, Thị trấn Kẻ Sặt, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9109309,
   "Latitude": 106.1478019
 },
 {
   "STT": 527,
   "Name": "Quầy thuốc số 09-CN CTCP Dược VTYT HD tại H.Bình Giang",
   "address": "Thôn Phủ, xã Thái Học, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8617209,
   "Latitude": 106.198027
 },
 {
   "STT": 528,
   "Name": "Quầy thuốc số 08-CN CTCP Dược VTYT HD tại H.Bình Giang",
   "address": "Số 50 Phạm Ngũ Lão, Thị trấn Kẻ Sặt, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9104853,
   "Latitude": 106.1479282
 },
 {
   "STT": 529,
   "Name": "Quầy thuốc số 06-CN CTCP Dược VTYT HD tại H.Bình Giang",
   "address": "Thôn Chanh Ngoài, xã Thúc Kháng, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8685432,
   "Latitude": 106.1522126
 },
 {
   "STT": 530,
   "Name": "Quầy thuốc số 02-CN CTCP Dược VTYT HD tại H.Bình Giang",
   "address": "Thôn Phủ, xã Thái Học, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8617209,
   "Latitude": 106.198027
 },
 {
   "STT": 531,
   "Name": "Quầy thuốc số 16-CN CTCP Dược VTYT HD tại H.Bình Giang",
   "address": "Chợ Phủ, xã Thái Học, Huyện  Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.870356,
   "Latitude": 106.1933837
 },
 {
   "STT": 532,
   "Name": "Quầy thuốc Linh Thúy",
   "address": "Số 44 Thống Nhất, Thị trấn Kẻ Sặt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9091802,
   "Latitude": 106.1469003
 },
 {
   "STT": 533,
   "Name": "Quầy thuốc Hiệp hương",
   "address": "số 57 khu Hạ,xã Tráng Liệt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.870356,
   "Latitude": 106.1933837
 },
 {
   "STT": 534,
   "Name": "Quầy thuốc Minh Dương",
   "address": "Thôn Hoàng Sơn, xã Thái Dương, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8441746,
   "Latitude": 106.1394419
 },
 {
   "STT": 535,
   "Name": "Quầy thuốc Thu Len",
   "address": "Ngọc Cục, xã vĩnh Hồng, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8987839,
   "Latitude": 106.1794814
 },
 {
   "STT": 536,
   "Name": "Quầy thuốc PK nội",
   "address": "khu I, Thị trấn Kẻ Sặt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.9136429,
   "Latitude": 106.1512911
 },
 {
   "STT": 537,
   "Name": "Quầy thuốc Phương Nga",
   "address": "Phủ, xã Thái Học, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.870356,
   "Latitude": 106.1933837
 },
 {
   "STT": 538,
   "Name": "quầy thuốc Thu Hiền",
   "address": "Phủ, xã Thái Học, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.870356,
   "Latitude": 106.1933837
 },
 {
   "STT": 539,
   "Name": "Đại lý bán thuốc Công ty TNHH dược phẩm Thành Đông",
   "address": "Xóm Mới, xã Bình Xuyên, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8372205,
   "Latitude": 106.1916808
 },
 {
   "STT": 540,
   "Name": "Đại  lý số 23",
   "address": "Thôn Hòa Loan, xã Nhân Quyền, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.870356,
   "Latitude": 106.1933837
 },
 {
   "STT": 541,
   "Name": "Đại lý số 5",
   "address": "Thôn Phủ, xã Thái Học, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.8617209,
   "Latitude": 106.198027
 },
 {
   "STT": 542,
   "Name": "Đại lý chi nhánh Công ty CPDược VTYT tại H.Bình Giang",
   "address": "Số 277 Trần Hưng Đạo, Thị trấn.Kẻ Sặt, Huyện Bình Giang,Tỉnh Hải Dương",
   "Longtitude": 20.909555,
   "Latitude": 106.1461639
 }
];