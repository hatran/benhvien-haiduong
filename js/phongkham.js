var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng Khám Sản Phụ Khoa Hải Dương",
   "address": "Kiot số 6, Đường Nguyễn Trãi, Thành phố Hải Dương",
   "Longtitude": 20.9408111,
   "Latitude": 106.3276707
 },
 {
   "STT": 2,
   "Name": "Phòng Khám Đa Khoa Thanh Bình Hải Dương",
   "address": "P. Thanh Bình, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.925173,
   "Latitude": 106.314005
 },
 {
   "STT": 3,
   "Name": "Phòng Khám Đa Khoa Quốc Tế Hải Dương",
   "address": "Tp. Ngô Quyền, P. Thanh Bình, Thành phố Hải Dương",
   "Longtitude": 20.9213239,
   "Latitude": 106.3157986
 },
 {
   "STT": 4,
   "Name": "Phòng khám phụ sản Bạch Mai",
   "address": "22 Phố Văn, Việt Hoà, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9429481,
   "Latitude": 106.3015378
 },
 {
   "STT": 5,
   "Name": "Phòng Khám Sản Phụ Khoa - Bs Nguyễn Đức Thơ",
   "address": "102 Nguyễn Văn Linh, P. Thanh Trung, Thành phố Hải Dương",
   "Longtitude": 20.9341769,
   "Latitude": 106.3158872
 },
 {
   "STT": 6,
   "Name": "Phòng Khám Chuyên Khoa Tai - Mũi - Họng",
   "address": "138 Lê Hồng Phong, P. Nguyễn Trãi, Thành phố Hải Dương",
   "Longtitude": 20.9378685,
   "Latitude": 106.3255924
 },
 {
   "STT": 7,
   "Name": "Nha Khoa Quốc Tế Chi Nhánh Hải Dương",
   "address": "125 Phạm Ngũ Lão, P. Phạm Ngũ Lão, Thành phố Hải Dương",
   "Longtitude": 20.9383188,
   "Latitude": 106.3216698
 },
 {
   "STT": 8,
   "Name": "Phòng Khám Đa Khoa Tín Đức",
   "address": "304 Nguyễn Lương Bằng, P. Thanh Trung, Thành phố Hải Dương",
   "Longtitude": 20.9360481,
   "Latitude": 106.293057
 },
 {
   "STT": 9,
   "Name": "Phòng khám chuyên khoa Da liễu - Bác sĩ Lan",
   "address": "số 4 Nguyễn Hải Thanh, P. Hải Tân, Thành phố Hải Dương",
   "Longtitude": 20.9302536,
   "Latitude": 106.3319988
 },
 {
   "STT": 10,
   "Name": "Phòng Khám Nhi - Bs Lê Thanh Duyên",
   "address": "11 Lý Thường Kiệt, P. Lê Thanh nghị, Thành phố Hải Dương",
   "Longtitude": 20.9366125,
   "Latitude": 106.3295357
 },
 {
   "STT": 11,
   "Name": "PHÒNG KHÁM BỆNH CHUYÊN KHOA DA LIỄU",
   "address": "SỐ 45 PHỐ Xuân Đài, P. Trần Phú, Thành phố Hải Dương",
   "Longtitude": 20.9386864,
   "Latitude": 106.3331126
 },
 {
   "STT": 12,
   "Name": "Phòng Khám Đức Dương",
   "address": "156 Nguyễn Trãi, P. Nguyễn Trãi, Thành phố Hải Dương",
   "Longtitude": 20.9377706,
   "Latitude": 106.3267767
 },
 {
   "STT": 13,
   "Name": "Phòng khám đa khoa Y cao hà nội",
   "address": "Số 5 đường Hoàng Ngân, P. Cẩm Thượng, Thành phố Hải Dương",
   "Longtitude": 20.952577,
   "Latitude": 106.322062
 },
 {
   "STT": 14,
   "Name": "Phòng Khám Mắt Bác Sĩ Thành",
   "address": "9 Hòa Bình, P. Bình Hàn, Thành phố Hải Dương",
   "Longtitude": 20.9466411,
   "Latitude": 106.3357419
 },
 {
   "STT": 15,
   "Name": "Nha khoa HTC",
   "address": "135 Ngô Quyền, P. Thanh Trung, Thành phố Hải Dương",
   "Longtitude": 20.9356775,
   "Latitude": 106.3146652
 },
 {
   "STT": 16,
   "Name": "Nha Khoa Ngọc Minh",
   "address": "1066 Lê Thanh Nghị, P. Hải Tân, Thành phố Hải Dương",
   "Longtitude": 20.9207161,
   "Latitude": 106.3205828
 },
 {
   "STT": 17,
   "Name": "Phòng khám đa khoa Thanh Bình",
   "address": "Trường Chinh, P. Thanh Bình, Thành phố Hải Dương",
   "Longtitude": 20.9270284,
   "Latitude": 106.3041609
 },
 {
   "STT": 18,
   "Name": "Phòng Khám Đa Khoa Bạch Đằng",
   "address": "P. Cẩm Thượng, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9483115,
   "Latitude": 106.314005
 },
 {
   "STT": 19,
   "Name": "Phòng Khám Đa Khoa Hữu Nghị Răng - Hàm - Mặt",
   "address": "118, Nguyễn Lương Bằng, Thành Phố Hải Dương",
   "Longtitude": 20.9379258,
   "Latitude": 106.3161222
 },
 {
   "STT": 20,
   "Name": "PHÒNG KHÁM - CHỮA BỆNH CHUYÊN KHOA DA LIỄU",
   "address": "SỐ 45 PHỐ XUÂN ĐÀI, P. Trần Phú, Thành phố Hải Dương",
   "Longtitude": 20.9386864,
   "Latitude": 106.3331126
 },
 {
   "STT": 21,
   "Name": "Nha Khoa Ngọc Tâm",
   "address": "326 Điện Biên Phủ, P. Bình Hàn, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.945402,
   "Latitude": 106.3219712
 },
 {
   "STT": 22,
   "Name": "Phòng Khám Đa Khoa Thái Thịnh",
   "address": "P. Bình Hàn, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9484628,
   "Latitude": 106.3272483
 },
 {
   "STT": 23,
   "Name": "Phòng Khám Nha khoa Đức",
   "address": "146 Nguyễn Lương Bằng, P. Phạm Ngũ Lão, Thành phố Hải Dương",
   "Longtitude": 20.9372516,
   "Latitude": 106.3138038
 },
 {
   "STT": 24,
   "Name": "Phòng Khám Đa Khoa Trường Xuân",
   "address": "P. Cẩm Thượng, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9483115,
   "Latitude": 106.314005
 },
 {
   "STT": 25,
   "Name": "Phòng Khám Đa Khoa Thanh Bình",
   "address": "49 Hồng Quang, P. Bình Hàn, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9458459,
   "Latitude": 106.3298118
 },
 {
   "STT": 26,
   "Name": "Phòng Khám Cương hằng",
   "address": "P. Thanh Trung, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9321067,
   "Latitude": 106.3022339
 },
 {
   "STT": 27,
   "Name": "Phòng Khám Đa Khoa Hiển Long - Bs Tuấn",
   "address": "Tiền Trung, Aí Quốc, Hải Dương",
   "Longtitude": 20.9674293,
   "Latitude": 106.3776265
 },
 {
   "STT": 28,
   "Name": "Phòng Khám Bs Nghiệp",
   "address": "71 Kim Sơn, P. Thanh Bình, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9232329,
   "Latitude": 106.3194253
 },
 {
   "STT": 29,
   "Name": "Nha Khoa Phương Linh",
   "address": "Khu 2, Phường Thạch Khôi, Thành Phố Hải Dương, Tỉnh Hải Dương",
   "Longtitude": 20.90475,
   "Latitude": 106.3129
 },
 {
   "STT": 30,
   "Name": "Phòng Khám Chữa Răng",
   "address": "74 Đồng Niên, Khu 5, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.94465,
   "Latitude": 106.3025
 },
 {
   "STT": 31,
   "Name": "Dịch Vụ Y Tế Chăm Sóc Sức Khỏe Răng Miệng",
   "address": "An Ninh, Thành Phố Hải Dương, Tỉnh Hải Dương",
   "Longtitude": 20.9449944,
   "Latitude": 106.3323295
 },
 {
   "STT": 32,
   "Name": "PHÒNG KHÁM NHA KHOA 95 NGUYỄN CHÍ THANH",
   "address": "95 Nguyễn Chí Thanh, P. Thanh Trung, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9330681,
   "Latitude": 106.3170658
 },
 {
   "STT": 33,
   "Name": "Phòng khám khoa nhi Bác Sĩ Sang",
   "address": "Nguyễn Văn Linh, P. Thanh Trung, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.9336148,
   "Latitude": 106.3065085
 },
 {
   "STT": 34,
   "Name": "Nha Khoa Kỹ Thuật Cao",
   "address": "119, Bạch Đằng, P. Trần Phú, Thành phố Hải Dương",
   "Longtitude": 20.9368241,
   "Latitude": 106.3383837
 },
 {
   "STT": 35,
   "Name": "Bác Sĩ chuyên khoa thần kinh",
   "address": "40 Hào Thành, P. Nguyễn Trãi, Thành phố Hải Dương",
   "Longtitude": 20.9389582,
   "Latitude": 106.3244712
 },
 {
   "STT": 36,
   "Name": "Nha Khoa Minh Tú",
   "address": "29 Lê Hồng Phong, P. Nguyễn Trãi, Thành phố Hải Dương",
   "Longtitude": 20.9399167,
   "Latitude": 106.3259428
 },
 {
   "STT": 37,
   "Name": "BS SƠN TAI MŨI HỌNG",
   "address": "18b Canh Nông, P. Quang Trung, Thành phố Hải Dương",
   "Longtitude": 20.9418335,
   "Latitude": 106.3344616
 },
 {
   "STT": 38,
   "Name": "NHA KHOA ĐINH TOÀN",
   "address": "Khuê Liễu, Thành phố Hải Dương, Hải Dương",
   "Longtitude": 20.913757,
   "Latitude": 106.3272483
 },
 {
   "STT": 39,
   "Name": "Nha Khoa Phấn Khởi",
   "address": "180 Thanh Niên, P. Quang Trung, Thành phố Hải Dương",
   "Longtitude": 20.9407042,
   "Latitude": 106.3366634
 },
 {
   "STT": 40,
   "Name": "Phòng khám sản phụ khoa Bác sĩ Hải",
   "address": "111B Phan Đình Phùng, P. Cẩm Thượng, Thành phố Hải Dương",
   "Longtitude": 20.9502971,
   "Latitude": 106.3195868
 },
 {
   "STT": 41,
   "Name": "Phòng Khám Chuyên Khoa Ung Bướu Bắc Sơn",
   "address": "11B Bắc Sơn, P. Quang Trung, Thành phố Hải Dương",
   "Longtitude": 20.9401768,
   "Latitude": 106.3318304
 },
 {
   "STT": 42,
   "Name": "Phòng Khám Da Liễu",
   "address": "22 Minh Khai, P. Trần Phú, Thành phố Hải Dương",
   "Longtitude": 20.9392262,
   "Latitude": 106.3306349
 },
 {
   "STT": 43,
   "Name": "Phòng Khám Nhi Khoa Ánh Dương",
   "address": "457 Điện Biên Phủ, P. Bình Hàn, Thành phố Hải Dương",
   "Longtitude": 20.9492685,
   "Latitude": 106.3237198
 }
];